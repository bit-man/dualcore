// main.cpp : Entry point de la aplicaci�n.
// v1.0.3 -02jun2005- nueva versi�n publicable
// v1.0.2 -10nov2004- nueva versi�n publicable
// v1.0.0 -24jun2004- 1ra versi�n publicable
// v0.1.1 -17jun2004- creado

#include <iostream>
#include <string>

#include "testing/util.h"
#include "testing/testsetdrv.h"
#include "testing/exception.h"
#include "testing/usageException.h"
#include "testing/fileFormatException.h"

#ifdef WIN32
	#include <tchar.h>								// MS-compilaci�n
#endif

using namespace std;

// todo: - estar�a bueno discriminar entre archivos de sets y de casos;
//			como para poder ejecutar directamente 1! caso.

#ifdef WIN32
	int _tmain(int argc, _TCHAR* argv[])			// versi�n microsoft
#else
	int main(int argc, char* argv[])				// versi�n est�ndar
#endif
{
	int imperfectCount = 0;
    try {

        if (argc != 2) throw UsageException("parametros invalidos");

		Histogram resultCount = TestSetDrv::exec(string(argv[1]));

		imperfectCount = resultCount[Result::error] + 
			resultCount[Result::unknown] + resultCount[Result::invalid];
		
		cout << "ok: " << resultCount[Result::ok] << '\t' 
			<< "error: " << resultCount[Result::error] << '\t' 
			<< "desconocido: " << resultCount[Result::unknown] << '\t' 
			<< "invalido: " << resultCount[Result::invalid] << '\t' 
			<< "total: " << resultCount[Result::ok] + imperfectCount
			<< endl;
			
        
    }
    catch (FileFormatException& e) {
		cerr << "formato invalido en " << e.fileName() << endl;
                cerr << e.message() << endl;
    }
	catch (UsageException& e) {
        cerr << e.message() << endl;
		cout << "uso: main <archivo_tests>\n";
    }
    catch (Exception& e) {
        cerr << e.message() << endl;
    }

#ifdef WIN32
	system("PAUSE");
#endif
	return imperfectCount;
}
