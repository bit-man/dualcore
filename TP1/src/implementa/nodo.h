#ifndef __NODO_H__
#define __NODO_H__

// Esto es necesario para poder usar ostream y su operador <<
#include <iostream>
#include <vector>

// Esto vuelca todo el namespace de la biblioteca standard al namespace global.
// Si se elimina habr� que poner 'std::' antes de cada nombre de la stdlib.
using namespace std;

template <class T> 
class Nodo {
	public:
		Nodo(const T &e);
		~Nodo();
	
		const T& dameElemento() const;
		Nodo<T>* dameElemAnt();
		Nodo<T>* dameElemPost();
		
        void seteoElemAnt(Nodo<T> *e);
        void seteoElemPost(Nodo<T> *e);

		template <class T2>
		friend ostream& operator<<(ostream&, const Nodo<T2>&);

	private:
		T elemento;
		Nodo<T>* elemAnt;
		Nodo<T>* elemPost;
};

template <class T> 
Nodo<T>::Nodo(const T &e)
{
	elemento = e;
	elemAnt = NULL;
	elemPost = NULL;
}

template <class T> 
Nodo<T>::~Nodo()
{
//el destructor por defecto...
}

template<class T>
const T& Nodo<T>::dameElemento() const
{
	return elemento;
}

//las funciones que devuelven datos y las que cambian...

template<class T>
Nodo<T>* Nodo<T>::dameElemAnt()
{
	return elemAnt;
}

template<class T>
Nodo<T>* Nodo<T>::dameElemPost()
{
	return elemPost;
}



template<class T>
void Nodo<T>::seteoElemAnt(Nodo<T> *e)
{
	elemAnt = e;

}


template<class T>
void Nodo<T>::seteoElemPost(Nodo<T> *e)
{
	elemPost = e;

}

//hago display por pantalla de cada parte de la estructura de Nodo
template<class T>
ostream& operator<<(ostream& out, const Nodo<T> &e)
{
	cout << "Elemento:    " << e.elemento << endl;
	cout << "Anterior:  " << e.elemAnt << endl;
	cout << "Posterior: " << e.elemPost << endl;
	return out;
}
#endif // __NODO_H__
