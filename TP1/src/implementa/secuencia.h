/***************************************************************************/
/*                                                                         */
/*                    Algoritmos y Estructuras de Datos II                 */
/*                              TP1  -  2C 2007                            */
/*                                                                         */
/*                 Clase Secuencia<elem>             Version 1.0           */
/*                 Lista doblemente enlazada                               */
/*                                                                         */
/***************************************************************************/

#ifndef __SECUENCIA_H__
#define __SECUENCIA_H__

// Esto es necesario para poder usar ostream y su operador <<
#include <iostream>
#include "nodo.h"

// Esto hace visible todo el namespace de la biblioteca standard.
// Evita tener que escribir 'std::' adelante de cada funcion de la stdlib.
using namespace std;

/***************************************************************************/
/*  Declaraci�n de la clase Secuencia<elem>                                */
/***************************************************************************/
template <class T> class Secuencia
{
	public:

		// Crea una secuencia vacia.
		Secuencia();

        // Crea una secuencia como copia de la otra. Si posteriormente se modifica un
		// elemento en una de las secuencias, no se deberia ver alterado en la otra.
		Secuencia(const Secuencia<T> &otra);

		// Destructor: debe eliminar toda la memoria pedida.
		~Secuencia();

		// Agrega un elemento al final de la secuencia.
		void agregarAtras(const T& e); 

		// Agrega un elemento al comienzo de la secuencia.
		void agregarAdelante(const T& e);

		// Devuelve el primer elemento de la secuencia.
		// Si la secuencia esta vacia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).
		T primero() const; 

		// Elimina el primer elemento de la secuencia.
		// Si la secuencia esta vacia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void fin(); 

		// Devuelve el ultimo elemento de la secuencia.
		// Si la secuencia esta vacia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).
		T ultimo() const; 

		// Elimina el ultimo elemento de la secuencia.
		// Si la secuencia esta vacia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void comienzo(); 

		// Indica si el elemento esta o no en la secuencia.
		bool esta(const T& e) const;

		// Elimina el elemento especificado de la secuencia.
		// Si el elemento se encuentra repetido, elimina la primera ocurrencia.
		// Si el elemento no esta en la secuencia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void quitar(const T& e);

		// Devuelve la cantidad de elementos de la secuencia.
		unsigned long longitud() const;

		// Agrega el elemento "nuevo" atras de la primera ocurrencia del elemento "atrasDe"
		// Si el elemento "atrasDe" no esta en la secuencia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void agregarAtrasDe(const T& nuevo, const T& atrasDe);

		// Agrega el elemento "nuevo" adelante de la primera ocurrencia del elemento "delanteDe"
		// Si el elemento "atrasDe" no esta en la secuencia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void agregarAdelanteDe(const T& nuevo, const T& delanteDe); 

		// Indica si la secuencia es igual a la pasada como parametro.
		// Dos secuencias son iguales si tienen los mismos elementos en el mismo orden.
		bool operator==(const Secuencia<T> &otra) const; 

		// Indica si la secuencia es distinta a la pasada como parametro.
		// Dos secuencias son distintas cuando no son iguales.
		bool operator!=(const Secuencia<T> &otra) const; 

		// Copia la secuencia pasada como parametro.
		// Luego de ejecutar esta operacion, ambas secuencias deberian ser iguales.
		// Esta funcion debe devolver una referencia al objeto (return *this;) para
		// permitir el encadenamiento de asignaciones.
		Secuencia<T>& operator=(const Secuencia<T> &otra);

		// Imprime la secuencia en forma legible.
		// Esta funcion deberia devolver una referencia al ostream (return o;) para
		// permitir el encadenamiento de operadores <<.
		template<class T2> friend ostream & operator<<(ostream &o, const Secuencia<T2> &secu);

		// Obtiene la secuencia desde un stream
		// Ya se da implementada para evitar trabajo extra.
		template<class T2> friend istream & operator>>(istream &is, Secuencia<T2> &s) {
			char nextChar;

			// Consume  el '[' de comienzo de secuencia
			is >> nextChar;
			assert(nextChar == '[');
			 
			// Lee elementos mientras no encuentre el ']'
			// (ver que hay un break)
			while(true) {
				// Pispea el pr�ximo car�cter para ver
				// si termin� la secuencia
				is >> nextChar;
				if(nextChar == ']')
					break;

				// Si no termin�, devuelve el car�cter que pispi�,
				// y delega en el operator>> de T2 para leer un
				// elemento
				is.putback(nextChar);
				T2 in;
				is >> in;

				// Agrega el elemento al final de la secuencia
				s.agregarAtras(in);
			}
			return is;
		}

	private:

		/*
		 * Devuelve un puntero al elemento pasado como par�metro.
		 * Tiene como pre-condici�n que el elemento debe estar 
		 * presente en la secuencia
		 */
		Nodo<T> *_obtenerPunteroAlElemento(const T& );
        
        /*
         * Inicializa las variables privadas
         */
        void _inicializaPrivate();
	
		Nodo<T> *primeroElem;	// puntero al primer elemento
		Nodo<T> *ultimoElem;	// puntero al �ltimo elemento
    	unsigned long cantElem;	// cantidad de elementos
	
};

template <class T> 
Secuencia<T>::Secuencia()
{
	_inicializaPrivate();                                            
}


template <class T> 
Secuencia<T>::Secuencia(const Secuencia<T> &otra)
{
     T elem;
     Nodo<T> *pElemOtra;

     _inicializaPrivate();

     pElemOtra = otra.primeroElem;
     while ( pElemOtra != NULL )
     {
        elem = pElemOtra->dameElemento();
        agregarAtras( elem );
        pElemOtra = pElemOtra->dameElemPost();
     }
}


template <class T> 
Secuencia<T>::~Secuencia()
{
    Nodo<T> *puntero;  //creo puntero para ir borrando nodos de la secuencia
    
    while ( primeroElem != NULL ) {

        puntero = primeroElem;  //asigno al puntero auxiliar el puntero del primer
	                             // elemento de la secuencia

        primeroElem = primeroElem->dameElemPost(); //coloco como primer nodo el siguiente
            				            //para poder borrar el nodo apuntado por puntero

        delete puntero; // libero la memoria usada por el nodo 
    }

}

template <class T> 
void Secuencia<T>::agregarAtras(const T& e)
{
  Nodo<T> *nuevoNodo = new Nodo<T> (e);
  nuevoNodo->seteoElemAnt(ultimoElem);

  if (ultimoElem != NULL)
     ultimoElem->seteoElemPost(nuevoNodo);

  if (primeroElem == NULL)    
    primeroElem = nuevoNodo;
  ultimoElem = nuevoNodo;
  cantElem++;    

}


template <class T> 
void Secuencia<T>::agregarAdelante(const T& e)
{
	Nodo<T> *nuevoNodo = new Nodo<T> (e);
	nuevoNodo->seteoElemPost(primeroElem);
	if (primeroElem != NULL)
	    primeroElem->seteoElemAnt(nuevoNodo);
	if (ultimoElem == NULL)    
	    ultimoElem = nuevoNodo;
	primeroElem = nuevoNodo;
	cantElem++;
}


template <class T> 
T Secuencia<T>::primero() const
{
	assert( longitud() != 0 );

	return primeroElem->dameElemento();              
}


template <class T> 
void Secuencia<T>::fin()
{
    assert( longitud() != 0 );
	
    Nodo<T> *puntero;
    puntero = primeroElem;          
    primeroElem = puntero->dameElemPost();

    if (primeroElem != NULL)
        primeroElem->seteoElemAnt(NULL);

    delete puntero;
    cantElem--;

    if (primeroElem == NULL)
            ultimoElem = NULL;
}

template <class T> 
T Secuencia<T>::ultimo() const
{	
	return ultimoElem->dameElemento();                         
}

template <class T> 
void Secuencia<T>::comienzo()
{
  assert( longitud() != 0 );

  if ( ! (ultimoElem == NULL) )
  {
	   Nodo<T> *puntero;
	   puntero = ultimoElem;                          
	   ultimoElem = puntero->dameElemAnt();
	   
	   if ( ultimoElem != NULL ) {
	   		ultimoElem->seteoElemPost(NULL);
  	   } else {
	   		primeroElem = NULL;
	   }
 
	   delete puntero;
	   cantElem--;
   }
   
     
}

template <class T> 
bool Secuencia<T>::esta(const T& e) const
{
	bool pertenece = false;
	Nodo<T> *p1;

	p1 = primeroElem;
	while ( (p1 != NULL)  && ! pertenece )
	{
	     pertenece = (p1->dameElemento() == e);
	     p1 = p1->dameElemPost();
	}
	return pertenece;
}

template <class T> 
void Secuencia<T>::quitar(const T& e)
{
    assert( esta(e) );
    
    Nodo<T> *pNodo;
    pNodo = _obtenerPunteroAlElemento( e );
    
    if ( pNodo->dameElemAnt() != NULL ) {
        pNodo->dameElemAnt()->seteoElemPost( pNodo->dameElemPost() );
    } else {
        // Ooops, es el primero de la secuencia
        primeroElem = pNodo->dameElemPost();
    };

    if ( pNodo->dameElemPost() != NULL ) {
        pNodo->dameElemPost()->seteoElemAnt( pNodo->dameElemAnt() );
    } else {
        // Ooops, es el �ltimo de la secuencia
        ultimoElem = pNodo->dameElemAnt();
    };

    delete pNodo;
    cantElem--;
}

template <class T> 
unsigned long Secuencia<T>::longitud() const
{
         return cantElem;
}

template <class T> 
void Secuencia<T>::agregarAtrasDe(const T& nuevo, const T& atrasDe)
{
	/*
	 * Al no estar definido el comportamiento si el elemento no est�,
	 * entonces tengo que asegurarme de no seguir ejecutando al menos
	 * si no se cumple esta condici�n
	 */
	assert( esta(atrasDe) );
	Nodo<T> *puntero;
	puntero = _obtenerPunteroAlElemento( atrasDe );

    if ( ultimoElem == puntero ) {
        agregarAtras(nuevo);        
    } else {
    	Nodo<T> *nuevoNodo = new Nodo<T>(nuevo);
    	nuevoNodo->seteoElemAnt(puntero);
        nuevoNodo->seteoElemPost(puntero->dameElemPost());
        puntero->dameElemPost()->seteoElemAnt(nuevoNodo);
        puntero->seteoElemPost(nuevoNodo);
        cantElem++;
    }
}

template <class T> 
void Secuencia<T>::agregarAdelanteDe(const T& nuevo, const T& delanteDe)
{

	/*
	 * Al no estar definido el comportamiento si el elemento no est�,
	 * entonces tengo que asegurarme de no seguir ejecutando al menos
	 * si no se cumple esta condici�n
	 */
	assert( esta(delanteDe) );
	Nodo<T> *puntero;
    puntero = _obtenerPunteroAlElemento( delanteDe );

    if ( primeroElem == puntero ) {
    	agregarAdelante( nuevo );
    } else {
    	Nodo<T> *nuevoNodo = new Nodo<T>(nuevo);
    	nuevoNodo->seteoElemPost( puntero );
	    nuevoNodo->seteoElemAnt( puntero->dameElemAnt() );
	    puntero->dameElemAnt()->seteoElemPost( nuevoNodo );
	    puntero->seteoElemAnt( nuevoNodo );
	    cantElem++;
    }     
}

template <class T> 
bool Secuencia<T>::operator==(const Secuencia<T> &otra) const
{
    if ( longitud() != otra.longitud() )
         return false;

    Nodo<T> *p1;
    Nodo<T> *p2;      
    p1 = primeroElem;
    p2 = otra.primeroElem;
    
    while ( p1!=NULL )
    {
        if ( p1->dameElemento() != p2->dameElemento() )
             return false;
    	p1 = p1->dameElemPost();
    	p2 = p2->dameElemPost();          
    
    }
    return true;
     
}

template <class T> 
bool Secuencia<T>::operator!=(const Secuencia<T> &otra) const
{
     return ! operator==(otra);
}

template <class T> 
Secuencia<T>& Secuencia<T>::operator=(const Secuencia<T> &otra)
{
  if(this == &otra)               // Evita copia sobre si mismo
        return *this;

   Secuencia<T> *secu = new Secuencia<T>(otra);
   return *secu; 
}

template<class T2> 
ostream & operator<<(ostream &o, const Secuencia<T2> &secu)
{
	o << "-----------------------------------------------------" << endl;
	o << "Secuencia : ";
	Nodo<T2> *ptr;
	ptr = secu.primeroElem;
	
	while ( ptr != NULL ) {
		o << ptr->dameElemento() << " ";
		ptr = ptr->dameElemPost();
	}	
	o << endl;
	o << "Longitud : " << secu.cantElem << endl;

	if ( secu.primeroElem != NULL ) {
		o << "Primer elemento : " << secu.primeroElem->dameElemento() << endl;
	}
	
	if ( secu.ultimoElem != NULL ) {
		o << "�ltimo elemento : " << secu.ultimoElem->dameElemento() << endl;
	}
		
	o << "-----------------------------------------------------" << endl;
    return o;
}

template <class T> 
Nodo<T> *Secuencia<T>::_obtenerPunteroAlElemento(const T& elem)
{
	Nodo<T> *puntero;                          
 	puntero = primeroElem;
 		
	while (puntero->dameElemento() != elem) {
    	puntero = puntero->dameElemPost();
	};

    return puntero;
}

template <class T> 
void Secuencia<T>::_inicializaPrivate() {
	primeroElem = NULL;
	ultimoElem =  NULL;
    cantElem = 0;    
};

#endif /* __SECUENCIA_H__ */

