#ifndef TIPOS_H
#define TIPOS_H

/* 
 * NOTA: es importante cuidar MUCHO el orden
 * en que se definen las inclusiones.
 */ 

// ---- TIPOS B�SICOS ----
// typedefs de tipos primitivos sueltos... 
#include "tiposBasicos.h"

//
// Todo lo que sigue no se necesita en este TP
// Se deja por motivos informativos.
//

// #includes de (tuplas de) tipos primitivos... 

// ---- COLECCIONES DE TIPOS B�SICOS ----
// algunas colecciones son tipos b�sicos,
// seg�n lo sea el elemento almacenado.
// #includes de contenedores para tipos b�sicos...
// #include "lib/conjlog.h"
// #include "lib/dicclog.h"
// #include "lib/secu.h"

// typedefs de colecciones de tipos b�sicos...
//
// Ejemplo:
//
// typedef conjlog<Animal> ConjAnimales;
// typedef ...
//
// (el driver usa estos tipos definidos como
// ConjAnimales, etc. Hay que definirlos)

#endif // TIPOS_H
