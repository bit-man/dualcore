// objectTable.h : tablas de s�mbolos para cada caso de test.
// v1.2.2 -04dic2005- corregido bug al liberar la tabla. �gracias alejandro!
// v1.2.1 -24oct2005- incorporados los cambios de adrian
// v1.1.0 -24jun2004- transformaci�n a clase
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -24may2004- creado

#ifndef OBJECT_TABLE_H
#define OBJECT_TABLE_H

#include <string>
//RR
#include <typeinfo>
#include <map>
#include "invalidTestException.h"
// FIXME: invalidTestException es muy propio del problema que estoy resolviendo
// habr�a que cambiarlo x algo m�s general.
#include "../implementa/tipos.h"
#include "id.h"

// la clase siguiente est� implementada fuera de ObjectTable para facilitar las
// primeras pruebas; podr�a estar bueno ponerla dentro o documentar x qu� no se
// puede hacerlo o no conviene.

class BaseHandler 
{
protected:
	void* ptr;
public:
	BaseHandler(void* objectPtr): ptr(objectPtr) { }
	virtual ~BaseHandler() { }
	virtual const type_info& innerType() = 0;
	void* const getPtr() { return ptr; }
};

template < typename T >
class Handler : public BaseHandler 
{
public:
	Handler(void* objectPtr): BaseHandler(objectPtr) { }
	~Handler() { delete static_cast<T*>(ptr); }
	const type_info& innerType() { return typeid(T); }
};

class ObjectTable 
{
private:

	map<string, BaseHandler*> store;
	
	// TODO: estar�a bueno tener un typedef para map::iterator. ser�a m�s claro
	// el c�digo de objectTable.cpp. no lo hago ahora para corregir el bug de 
	// liberar la tabla modificando un �nico archivo.
		
public:

	ObjectTable();
	~ObjectTable();

	// si alguna vez funciona el 'export', separar imple de encabezado.
	template < typename T >
	ObjectTable& define(const Id<T>& id, T* objectPtr) throw (InvalidTestException)
	{
		if (store.find(id.name) != store.end())
			throw InvalidTestException("el identificador ya figura en la tabla de simbolos: " + id.name);
			// al capturar esta excepci�n se deberia disponer de info contextual.
			// en lugar de bajar esa info, subir la excepci�n y que cada cual 
			// agregue lo suyo.

		store[id.name] = new Handler<T>(objectPtr);
		return *this;
	}

	template < typename T >
	T& operator [](const Id<T>& id) throw (InvalidTestException)
	// TODO: generalizar InvalidTestException.
	{
		if (store.find(id.name) == store.end())
			throw InvalidTestException("el identificador no figura en la tabla de simbolos: " + id.name);
		// al capturar esta excepci�n se deberia agregar info con la situacion 
		// desde la cual se lleg� hasta esta condici�n de error. en lugar de 
		// bajar info de contexto, parece mejor subir la excepci�n y que cada 
		// cual agregue lo suyo.

		BaseHandler* handler = store[id.name];
		if (handler->innerType() != typeid(T))
			throw InvalidTestException(string("el tipo del identificador (") + 
					typeid(T).name() + ") no coincide con el tipo almacenado ("+
					handler->innerType().name() + ")." +
					"\nid.name=" + id.name
					);
		return *(static_cast<T*>(store[id.name]->getPtr()));
	}
};

#endif
