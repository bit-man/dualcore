// testsetdrv.h : maneja el conjunto de casos de test.
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -20may2004- testsetdrv.cpp: creado

#ifndef TEST_SET_DRV_H
#define TEST_SET_DRV_H

#include <map>
#include <string>
#include "result.h"
#include "usageException.h"
#include "fileFormatException.h"

typedef map<Result::Type, int> Histogram;

namespace TestSetDrv
{
	using std::string;

	Histogram exec(const string& fileName) 
		throw(UsageException, FileFormatException, Exception);
}

#endif
