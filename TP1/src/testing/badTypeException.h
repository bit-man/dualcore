// badTypeException.h : 
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -23may2004- creado

#ifndef BAD_TYPE_EXCEPTION_H
#define BAD_TYPE_EXCEPTION_H

#include "exception.h"

class BadTypeException : public Exception
{
public:
	BadTypeException(const string message = ""): Exception(message) { }
};

#endif
