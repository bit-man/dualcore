// v1.3.0

#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include "cmd.h"
#include "util.h"
#include "../implementa/secuencia.h"
#include "id.h"

using std::string;

enum Type { 			
	// -- TAD Secuencia  --------------
	vacia,
	crearPorCopia,
	crearAsignando,
	agregarAtras,
	agregarAdelante,
	primero,
	fin,
	ultimo,
	comienzo,
	esta,
	quitar,
	longitud,
	agregarAtrasDe,
	agregarAdelanteDe,
	iguales,
	distintas
};

class Decoder 
{
private:    
	std::map<string, Type> codes;
public:    	
	Decoder();
    Type operator()(string cmdTypeStr) const throw (BadTypeException) 
    {
    	if (codes.find(toupperstr(cmdTypeStr)) == codes.end())
			throw BadTypeException("comando desconocido: " + cmdTypeStr);
        return codes.find(cmdTypeStr)->second;
	}
};

void cmd::exec(string cmdln, ObjectTable& symbolTable, Result& result) 
	throw (UsageException, BadTypeException, InvalidTestException, 
		ErrorTestException, std::ios_base::failure)
// TODO rastrear d�nde se usa InvalidTestException, si es que se usa,
// y corregir si no. deber�a servir para cuando est� malformado un test,
// como xej cuando hay error de tipos o mala cantidad de argumentos.
{
	if (result.getType() != Result::ok and result.getType() != Result::unknown)
		throw UsageException("Se quiere ejecutar un comando luego de una situacion irrecuperable");

	istringstream ist(cmdln);	
	ist.exceptions(ios_base::badbit | ios_base::failbit);
	boolalpha(ist);						// usar true y false en vez de 1 y 0
	string cmd;
	ist >> cmd;
	Decoder decode;

	// declaraci�n de variables que figuran en los comandos
	Id< Secuencia<nat> > idSecuencia, idOtraSecuencia;

	nat aBuscar, elemento, esperado, obtenido;
	Secuencia<nat> secuenciaEsperada, secuenciaObtenida, secuencia;
	bool boolEsperado, boolObtenido;

	switch(decode(cmd)) {

	/* comentarios: 
	 * 	
	 * - cuando un comando recibe o devuelve tipos primitivos, lo hace 
	 * 	 siempre por copia; mientras que cuando manipula tipos complejos lo 
	 *   hace siempre por referencia.
	 * - las funciones que en los TADs reciben una instancia de un tipo 
	 * 	 complejo y devuelven otra, en la implementaci�n reciben el 
	 * 	 par�metro por referencia y lo modifican.
	 * - al modificar un par�metro complejo recibido por referencia, 
	 *   probablemente se aprovechen efectos colaterales del aliasing.
	 * - los nombres son casesensitive.
	 * - no se verifica que se cumplan las precondiciones.
	 * - todo lo que siga a continuaci�n de los comandos necesarios, se 
	 * 	 ignora.
	 * - los generadores no recursivos se implementaron como constructores.
	 * - los generadores recursivos NO se implementaron como constructores,
	 *   sino como funciones miembro.
	 * - como siempre, si surgen, preguntar.
	 */
	 
	/* TODO:
	 * - verificar que se detectan efectivamente los par�metros de menos.
	 * - verificar qu� pasa con par�metros de m�s.
	 * - podr�a tratar de integrar el criterio a las anotaciones.
	 * - podr�a agregarse una anotaci�n para constructores de tipos 
	 * 	 auxiliares, considerando en especial los casos Tupla y Colecci�n.
	 * - podr�a agregarse una anotaci�n sobre los constructores por defecto.
	 */
	
	// -- Secuencia -----------------------
	// observadores

	case primero: // Secuencia -> nat
		// primero idSecuencia nat
		ist >> idSecuencia >> esperado;
		obtenido = symbolTable[idSecuencia].primero(); 
		assertEquals(esperado, obtenido);
		break;
	case ultimo: // Secuencia -> nat
		// ultimo idSecuencia nat
		ist >> idSecuencia >> esperado;
		obtenido = symbolTable[idSecuencia].ultimo(); 
		assertEquals(esperado, obtenido);
		break;
	case esta: // Secuencia x elem -> bool
		// esta idSecuencia bool
		ist >> idSecuencia >> aBuscar >> boolEsperado;
		boolObtenido = symbolTable[idSecuencia].esta(aBuscar);
		assertEquals(boolEsperado, boolObtenido);
		break;
	case longitud: // Secuencia -> nat
		// longitud idSecuencia nat
		ist >> idSecuencia >> esperado;
		obtenido = symbolTable[idSecuencia].longitud();
		assertEquals(esperado, obtenido);
		break;

	//generadores
	case vacia: // -> Secuencia
		ist >> idSecuencia;
		symbolTable.define(idSecuencia,
				new Secuencia<nat>());
		break;
	case crearPorCopia: // Secuencia -> Secuencia
		ist >> idSecuencia >> idOtraSecuencia;
		symbolTable.define(idSecuencia,
				new Secuencia<nat>(symbolTable[idOtraSecuencia]));
		break;
	// rrela Sun Aug 26 00:02:41 ART 2007
	// Esto me hace aliasing aunque la imple de secuencia
	// no lo haga. Quise ponerlo para poder testear
	// aliasing de operator=, pero no me anda.
	case crearAsignando: // Secuencia -> Secuencia
		ist >> idSecuencia >> idOtraSecuencia;
		secuencia = symbolTable[idOtraSecuencia];
		symbolTable.define(idSecuencia, &secuencia);
		break;
	case agregarAtras: // Secuencia x elem -> Secuencia
		// agregarAtras idSecuencia elem
		ist >> idSecuencia >> elemento;
		symbolTable[idSecuencia].agregarAtras(elemento); 
		break;
	case agregarAdelante: // Secuencia x elem -> Secuencia
		// agregarAdelante idSecuencia elem
		ist >> idSecuencia >> elemento;
		symbolTable[idSecuencia].agregarAdelante(elemento); 
		break;
	case agregarAtrasDe: // Secuencia x elem x elem -> Secuencia
		// agregarAtrasDe idSecuencia elem elem
		ist >> idSecuencia >> elemento >> aBuscar;
		symbolTable[idSecuencia].agregarAtrasDe(elemento, aBuscar); 
		break;
	case agregarAdelanteDe: // Secuencia x elem x elem -> Secuencia
		// agregarAdelante idSecuencia elem elem
		ist >> idSecuencia >> elemento >> aBuscar;
		symbolTable[idSecuencia].agregarAdelanteDe(elemento, aBuscar); 
		break;
	case quitar: // Secuencia x elem -> Secuencia
		// agregarAdelante idSecuencia elem
		ist >> idSecuencia >> elemento;
		symbolTable[idSecuencia].quitar(elemento); 
		break;
	case fin: // Secuencia -> Secuencia
		// fin idSecuencia
		ist >> idSecuencia;
		symbolTable[idSecuencia].fin();
		break;
	case comienzo: // Secuencia -> Secuencia
		// comienzo idSecuencia
		ist >> idSecuencia;
		symbolTable[idSecuencia].comienzo();
		break;

	// auxiliares
	case iguales: // Secuencia x Secuencia -> bool
		// iguales idSecuencia idSecuencia bool
		ist >> idSecuencia >> idOtraSecuencia >> boolEsperado;
		boolObtenido =
			symbolTable[idSecuencia] == symbolTable[idOtraSecuencia];
		assertEquals(boolEsperado, boolObtenido);
		break;
	case distintas: // Secuencia x Secuencia -> bool
		// distintas idSecuencia idSecuencia bool
		ist >> idSecuencia >> idOtraSecuencia >> boolEsperado;
		boolObtenido =
			symbolTable[idSecuencia] != symbolTable[idOtraSecuencia];
		assertEquals(boolEsperado, boolObtenido);
		break;

	default:
		throw UsageException("No est� contemplado el comando: " + cmd);
	}
}

Decoder::Decoder() 
{
	// �CUIDADO! todas las cadenas deben estar en may�sculas.
	
	// -- TAD Secuencia ---------------
	// OBSERVADORES
	codes["PRIMERO"]            = primero;
	codes["ULTIMO"]				= ultimo;
	codes["FIN"]                = fin;
	codes["COMIENZO"]			= comienzo;
	codes["ESTA"]               = esta;
	codes["LONGITUD"]           = longitud;

	// GENERADORES
	codes["VACIA"]              = vacia;
	codes["CREARPORCOPIA"]      = crearPorCopia;
	codes["CREARASIGNANDO"]     = crearAsignando;
	codes["AGREGARATRAS"]       = agregarAtras;
	codes["AGREGARADELANTE"]    = agregarAdelante;
	codes["AGREGARATRASDE"]     = agregarAtrasDe;
	codes["AGREGARADELANTEDE"]  = agregarAdelanteDe;
	codes["QUITAR"]             = quitar;

	// AUXILIARES
	codes["IGUALES"]            = iguales;
	codes["DISTINTAS"]          = distintas;
}

// vim:ts=4:sw=4:
