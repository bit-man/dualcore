// invalidTestException.h : 
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -25may2004- creado

#ifndef INVALID_TEST_EXCEPTION_H
#define INVALID_TEST_EXCEPTION_H

#include "exception.h"

class InvalidTestException : public Exception
{
public:
	InvalidTestException(string message = ""): Exception(message) { }
};

#endif
