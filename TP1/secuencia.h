/***************************************************************************/
/*                                                                         */
/*                    Algoritmos y Estructuras de Datos II                 */
/*                              TP1  -  2C 2007                            */
/*                                                                         */
/*                 Clase Secuencia<elem>             Version 1.0           */
/*                 Lista doblemente enlazada                               */
/*                                                                         */
/***************************************************************************/

#ifndef __SECUENCIA_H__
#define __SECUENCIA_H__

// Esto es necesario para poder usar ostream y su operador <<
#include <iostream>


// Esto hace visible todo el namespace de la biblioteca standard.
// Evita tener que escribir 'std::' adelante de cada funcion de la stdlib.
using namespace std;

/***************************************************************************/
/*  Declaraci�n de la clase Secuencia<elem>                                */
/***************************************************************************/
template <class T> class Secuencia
{
	public:

		// Crea una secuencia vacia.
		Secuencia();

		// Crea una secuencia como copia de la otra. Si posteriormente se modifica un
		// elemento en una de las secuencias, no se deberia ver alterado en la otra.
		Secuencia(const Secuencia<T> &otra);

		// Destructor: debe eliminar toda la memoria pedida.
		~Secuencia();

		// Agrega un elemento al final de la secuencia.
		void agregarAtras(const T& e); 

		// Agrega un elemento al comienzo de la secuencia.
		void agregarAdelante(const T& e);

		// Devuelve el primer elemento de la secuencia.
		// Si la secuencia esta vacia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).
		T primero() const; 

		// Elimina el primer elemento de la secuencia.
		// Si la secuencia esta vacia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void fin(); 

		// Devuelve el ultimo elemento de la secuencia.
		// Si la secuencia esta vacia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).
		T ultimo() const; 

		// Elimina el ultimo elemento de la secuencia.
		// Si la secuencia esta vacia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void comienzo(); 

		// Indica si el elemento esta o no en la secuencia.
		bool esta(const T& e) const;

		// Elimina el elemento especificado de la secuencia.
		// Si el elemento se encuentra repetido, elimina la primera ocurrencia.
		// Si el elemento no esta en la secuencia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void quitar(const T& e);

		// Devuelve la cantidad de elementos de la secuencia.
		unsigned long longitud() const;

		// Agrega el elemento "nuevo" atras de la primera ocurrencia del elemento "atrasDe"
		// Si el elemento "atrasDe" no esta en la secuencia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void agregarAtrasDe(const T& nuevo, const T& atrasDe);

		// Agrega el elemento "nuevo" adelante de la primera ocurrencia del elemento "delanteDe"
		// Si el elemento "atrasDe" no esta en la secuencia, el comportamiento no esta definido 
		// (puede hacer cualquier cosa).		
		void agregarAdelanteDe(const T& nuevo, const T& delanteDe); 

		// Indica si la secuencia es igual a la pasada como parametro.
		// Dos secuencias son iguales si tienen los mismos elementos en el mismo orden.
		bool operator==(const Secuencia<T> &otra) const; 

		// Indica si la secuencia es distinta a la pasada como parametro.
		// Dos secuencias son distintas cuando no son iguales.
		bool operator!=(const Secuencia<T> &otra) const; 

		// Copia la secuencia pasada como parametro.
		// Luego de ejecutar esta operacion, ambas secuencias deberian ser iguales.
		// Esta funcion debe devolver una referencia al objeto (return *this;) para
		// permitir el encadenamiento de asignaciones.
		Secuencia<T>& operator=(const Secuencia<T> &otra);

		// Imprime la secuencia en forma legible.
		// Esta funcion deberia devolver una referencia al ostream (return o;) para
		// permitir el encadenamiento de operadores <<.
		template<class T2> friend ostream & operator<<(ostream &o, const Secuencia<T2> &secu);

		// Obtiene la secuencia desde un stream
		// Ya se da implementada para evitar trabajo extra.
		template<class T2> friend istream & operator>>(istream &is, Secuencia<T2> &s) {
			char nextChar;

			// Consume  el '[' de comienzo de secuencia
			is >> nextChar;
			assert(nextChar == '[');
			 
			// Lee elementos mientras no encuentre el ']'
			// (ver que hay un break)
			while(true) {
				// Pispea el pr�ximo car�cter para ver
				// si termin� la secuencia
				is >> nextChar;
				if(nextChar == ']')
					break;

				// Si no termin�, devuelve el car�cter que pispi�,
				// y delega en el operator>> de T2 para leer un
				// elemento
				is.putback(nextChar);
				T2 in;
				is >> in;

				// Agrega el elemento al final de la secuencia
				s.agregarAtras(in);
			}
			return is;
		}

	private:
		// A completar con la implementaci�n de cada uno ...
	
};

#endif /* __SECUENCIA_H__ */

