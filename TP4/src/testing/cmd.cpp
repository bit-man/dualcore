// v1.3.0

#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include "cmd.h"
#include "util.h"
#include "../implementa/universo.h"
#include "id.h"

using std::string;

enum Type { 			
	// -- TAD Galaxia --------------
    crear, agSat, centro, sats, esSat,
	// -- TAD Universo -------------
    bigBang, agTostadora, abrirAgujero, ordenarViaje,
	esGalaxia, esTostadora, pos, viajes, hayAgujero, energia,
	hayCampeona, campeona, planetas, visitas, masVisitado,
    // -- diccionario implementado sobre array con funci�n de hash
    diccCteCrear, diccCteClaves,
    // -- conjunto de complejidades logaritmicas
    conjStrCrear, conjStrAg, conjStrEq
};

class Decoder 
{
private:    
	std::map<string, Type> codes;
public:    	
	Decoder();
    Type operator()(string cmdTypeStr) const throw (BadTypeException) 
    {
    	if (codes.find(toupperstr(cmdTypeStr)) == codes.end())
			throw BadTypeException("comando desconocido: " + cmdTypeStr);
        return codes.find(cmdTypeStr)->second;
	}
};

void cmd::exec(string cmdln, ObjectTable& symbolTable, Result& result) 
	throw (UsageException, BadTypeException, InvalidTestException, 
		ErrorTestException, std::ios_base::failure)
// TODO rastrear d�nde se usa InvalidTestException, si es que se usa,
// y corregir si no. deber�a servir para cuando est� malformado un test,
// como xej cuando hay error de tipos o mala cantidad de argumentos.
{
	if (result.getType() != Result::ok and result.getType() != Result::unknown)
		throw UsageException("Se quiere ejecutar un comando luego de una situacion irrecuperable");

	istringstream ist(cmdln);	
	ist.exceptions(ios_base::badbit | ios_base::failbit);
	boolalpha(ist);						// usar true y false en vez de 1 y 0
	string cmd;
	ist >> cmd;
	Decoder decode;

	// declaraci�n de variables que figuran en los comandos
	Id<Galaxia> idGalaxia;
	Id<Universo> idUniverso;
    Id<ConjPlaneta> idConjPlaneta;

    Galaxia galaxia;
	Planeta planetaEsperado, planetaObtenido, planeta, satelite;
	ConjPlaneta conjPlanetaEsperado, conjPlanetaObtenido, conjPlaneta;
    ConjGalaxia *conjGalaxia;
    ConjIdGalaxia conjIdGalaxia;
	Tostadora tostadoraEsperada, tostadoraObtenida, tostadora;
	Nat natEsperado, natObtenido;
	bool boolEsperado, boolObtenido;
	Agujero agujero;

    //Pruebas tipos basicos
    Id<diccCte<string,int> > idDiccCte;
    Id<conjlog<string> > idConjStr, idConjStr1, idConjStr2;
    string nStr;
    string sString;

	switch(decode(cmd)) {

	/* comentarios: 
	 * 	
	 * - cuando un comando recibe o devuelve tipos primitivos, lo hace 
	 * 	 siempre por copia; mientras que cuando manipula tipos complejos lo 
	 *   hace siempre por referencia.
	 * - las funciones que en los TADs reciben una instancia de un tipo 
	 * 	 complejo y devuelven otra, en la implementaci�n reciben el 
	 * 	 par�metro por referencia y lo modifican.
	 * - al modificar un par�metro complejo recibido por referencia, 
	 *   probablemente se aprovechen efectos colaterales del aliasing.
	 * - los nombres son casesensitive.
	 * - no se verifica que se cumplan las precondiciones.
	 * - todo lo que siga a continuaci�n de los comandos necesarios, se 
	 * 	 ignora.
	 * - los generadores no recursivos se implementaron como constructores.
	 * - los generadores recursivos NO se implementaron como constructores,
	 *   sino como funciones miembro.
	 * - como siempre, si surgen, preguntar.
	 */
	 
	/* TODO:
	 * - verificar que se detectan efectivamente los par�metros de menos.
	 * - verificar qu� pasa con par�metros de m�s.
	 * - podr�a tratar de integrar el criterio a las anotaciones.
	 * - podr�a agregarse una anotaci�n para constructores de tipos 
	 * 	 auxiliares, considerando en especial los casos Tupla y Colecci�n.
	 * - podr�a agregarse una anotaci�n sobre los constructores por defecto.
	 */

	// -- Galaxia  -----------------------
	// observadores
	case centro:
		// centro Id<Galaxia> Planeta
		ist >> idGalaxia >> planetaEsperado;
		planetaObtenido = symbolTable[idGalaxia].centro(); 
		assertEquals(planetaEsperado, planetaObtenido);  
		break;
	case sats:
		// sats Id<Galaxia> Planeta ConjPlaneta
		ist >> idGalaxia >> planeta >> conjPlanetaEsperado;
		conjPlanetaObtenido = symbolTable[idGalaxia].sats(planeta); 
		assertEquals(conjPlanetaEsperado, conjPlanetaObtenido);
	 	break;
	case esSat:
		// esSat Id<Galaxia> Planeta Planeta bool
		ist >> idGalaxia >> planeta >> satelite >> boolEsperado;
		boolObtenido = symbolTable[idGalaxia].esSat(planeta,satelite); 
		assertEquals(boolEsperado, boolObtenido);
	 	break;
    /** Descomentar si deciden usarla
	case planetas:
		// planetas Id<Galaxia> ConjPlaneta
		ist >> idGalaxia >> conjPlanetaEsperado;
		conjPlanetaObtenido = symbolTable[idGalaxia].planetas(); 
		assertEquals(conjPlanetaEsperado, conjPlanetaObtenido);
	 	break;
    */

	//generadores
	case crear:
		// crear Planeta Id<Galaxia>
		ist >> planeta >> idGalaxia;
		symbolTable.define(idGalaxia, new Galaxia(planeta));
		break;
	case agSat:
		// agSat Id<Galaxia> Planeta Planeta
		ist >> idGalaxia >> planeta >> satelite;
		symbolTable[idGalaxia].agSat(planeta, satelite);
		break;

	// -- Universo -----------------------
	// observadores
	case esGalaxia:
		// esGalaxia Id<Universo> id<Galaxia> bool
		ist >> idUniverso >> idGalaxia >> boolEsperado;
        // Asquito para que no pinche cuando
        // traduce el ID a Galaxia
        galaxia = Galaxia();
        try {
            galaxia = symbolTable[idGalaxia];
        } catch ( InvalidTestException ite ) {
            // Ser m�s especifico: cachear s�lo
            // "el identificador no figura en la tabla de simbolos: "
        }
		boolObtenido = symbolTable[idUniverso].esGalaxia(galaxia); 
		assertEquals(boolEsperado, boolObtenido);  
		break;
	case esTostadora:
		// esTostadora Id<Universo> Tostadora bool
		ist >> idUniverso >> tostadora >> boolEsperado;
		boolObtenido = symbolTable[idUniverso].esTostadora(tostadora); 
		assertEquals(boolEsperado, boolObtenido);
	 	break;
	case pos:
		// pos Id<Universo> Tostadora Planeta
		ist >> idUniverso >> tostadora >> planetaEsperado;
		planetaObtenido = symbolTable[idUniverso].pos(tostadora); 
		assertEquals(planetaEsperado, planetaObtenido);
	 	break;
	case viajes:
		// viajes Id<Universo> Tostadora Nat
		ist >> idUniverso >> tostadora >> natEsperado;
		natObtenido = symbolTable[idUniverso].viajes(tostadora); 
		assertEquals(natEsperado, natObtenido);
	 	break;
	case hayAgujero:
		// hayAgujero Id<Universo> Agujero boolean
		ist >> idUniverso >> agujero >> boolEsperado;
		boolObtenido = symbolTable[idUniverso].hayAgujero(agujero); 
		assertEquals(boolEsperado, boolObtenido);
	 	break;
	case energia:
		// energia Id<Universo> Agujero Nat
		ist >> idUniverso >> agujero >> natEsperado;
		natObtenido = symbolTable[idUniverso].energia(agujero); 
		assertEquals(natEsperado, natObtenido);
	 	break;
	case hayCampeona:
		// hayCampeona Id<Universo> bool
		ist >> idUniverso >> boolEsperado;
		boolObtenido = symbolTable[idUniverso].hayCampeona(); 
		assertEquals(boolEsperado, boolObtenido);
	 	break;
	case campeona:
		// campeona Id<Universo> Tostadora
		ist >> idUniverso >> tostadoraEsperada;
		tostadoraObtenida = symbolTable[idUniverso].campeona(); 
		assertEquals(tostadoraEsperada, tostadoraObtenida);
	 	break;
    /** Descomentar para rtp3
	case visitas:
		// visitas Id<Universo> Planeta Nat
		ist >> idUniverso >> planeta >> natEsperado;
		natObtenido = symbolTable[idUniverso].visitas(planeta); 
		assertEquals(natEsperado, natObtenido);
	 	break;
	case masVisitado:
		// masVisitado Id<Universo> Planeta
		ist >> idUniverso >> planetaEsperado;
		planetaObtenido = symbolTable[idUniverso].masVisitado(); 
		assertEquals(planetaEsperado, planetaObtenido);
	 	break;
    */

	//generadores
	case bigBang:
		// bigBang ConjIdGalaxia Id<Universo>
		ist >> conjIdGalaxia >> idUniverso;
            // Chanchada diferente a lo normal:
            // Para no tener que escribir conjunto de
            // galaxias en un stream, me pasan un conjunto
            // de los ids de galaxias ya creadas y yo los
            // convierto en galaxias. 
            {
                conjGalaxia = new ConjGalaxia();
                IteradorConjIdGalaxia it(conjIdGalaxia);
                it.comenzar();
                while (it.valido())
                {
                    Id<Galaxia> idg = it.actual();
                    conjGalaxia->agregar(symbolTable[idg]);
                    it.siguiente();
                }
                //delete conjGalaxia;
            }
		symbolTable.define(idUniverso, new Universo(*conjGalaxia));
		delete conjGalaxia;
		break;
	case agTostadora:
		// agTostadora Id<Universo> Tostadora Planeta
		ist >> idUniverso >> tostadora >> planeta;
		symbolTable[idUniverso].agTostadora(tostadora, planeta);
		break;
	case abrirAgujero:
		// abrirAgujero Id<Universo> Agujero
		ist >> idUniverso >> agujero;
		symbolTable[idUniverso].abrirAgujero(agujero);
		break;
	case ordenarViaje:
		// ordenarViaje Id<Universo> Tostadora Planeta
		ist >> idUniverso >> tostadora >> planeta;
		symbolTable[idUniverso].ordenarViaje(tostadora, planeta);
		break;

	// -- comandos auxiliares -------------
	// case crearConjPlanetas:
	//   	conjPlaneta = new ConjPlaneta();
	//   	ist >> *conjPlaneta >> idConjPlaneta;
	//   	symbolTable.define(idConjPlaneta, conjPlaneta);
	//   	break;
    
    
    // -- diccCte -----------------------
	// generador
    case diccCteCrear:
        // diccCteCrear IdDiccionario
        ist >> idDiccCte;
		symbolTable.define(idDiccCte, new diccCte<string,int>());
		break;

	case diccCteClaves:
		// diccCteClaves IdDiccionario IdConjunto
		ist >> idDiccCte >> idConjStr; 
		{
			conjlog<string>& k = symbolTable[idDiccCte].claves();
			symbolTable.define(idConjStr, new conjlog<string>( k ) );
			delete &k;
		}
		break;
    
    case conjStrCrear:
        // conjIntCrear IdConjunto
        ist >> idConjStr;
		symbolTable.define(idConjStr, new conjlog<string>() );
		break;

	case conjStrAg:
		// conjIntAg IdConjunto Number
		ist >> idConjStr >> nStr;
		symbolTable[idConjStr].agregar(nStr);
		break;

	case conjStrEq:
		// conjStrEq IdConjunto1 IdConjunto2
		ist >> idConjStr1 >> idConjStr2;
        assertEquals( symbolTable[idConjStr1], symbolTable[idConjStr2] );
		break;
         
	default:
		throw UsageException("No est� contemplado el comando: " + cmd);
	}
}

Decoder::Decoder() 
{
	// �CUIDADO! todas las cadenas deben estar en may�sculas.
	
	// -- TAD Galaxia ---------------
	// OBSERVADORES BASICOS
	codes["CENTRO"]   =  centro;
	codes["SATS"]     =  sats;
	codes["ESSAT"]    =  esSat;

	// GENERADORES
	codes["CREAR"]    =  crear;
	codes["AGSAT"]    =  agSat;
	
	// OTRAS OPERACIONES

	// -- TAD Universo --------------
	// OBSERVADORES BASICOS
	codes["ESGALAXIA"]    =  esGalaxia;
	codes["ESTOSTADORA"]  =  esTostadora;
	codes["POS"]          =  pos;
	codes["VIAJES"]       =  viajes;
	codes["HAYAGUJERO"]   =  hayAgujero;
	codes["ENERGIA"]      =  energia;
	codes["HAYCAMPEONA"]  =  hayCampeona;
	codes["CAMPEONA"]     =  campeona;
	codes["PLANETAS"]     =  planetas;
    codes["VISITAS"]      =  visitas;
    codes["MASVISITADO"]  =  masVisitado;

	// GENERADORES
	codes["BIGBANG"]      =  bigBang;
	codes["AGTOSTADORA"]  =  agTostadora;
	codes["ABRIRAGUJERO"] =  abrirAgujero;
	codes["ORDENARVIAJE"] =  ordenarViaje;
    
	// -- diccCte --------------
	codes["DICCCTECREAR"] = diccCteCrear;
	codes["DICCCTECLAVES"] = diccCteClaves;
    
	codes["CONJSTRCREAR"] = conjStrCrear;
	codes["CONJSTRAG"]    = conjStrAg;
    codes["CONJSTREQ"]    = conjStrEq;
    
	// -- comandos auxiliares -------
	// codes["CREARCONJPLANETA"]  =  crearConjPlaneta;
}

// vim:ts=4:sw=4:
