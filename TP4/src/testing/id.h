// id.h :
// v1.0.0 -24oct2004- creado y publicable

#ifndef ID_H
#define ID_H

template < typename T >
class Id 
{
public:
	string name;
	
    template<typename R> friend 
    std::istream& operator>>(std::istream& is, Id<R>&id);

    bool operator==(const Id& otro) const { return name == otro.name; }
    bool operator!=(const Id& otro) const { return !(*this == otro); }
    bool operator<(const Id& otro) const { return name < otro.name; }
};


// TODO: si se pudiera separar imple de encabezado, arreglar ac�.
template < typename T >
std::istream& operator>>(std::istream& is, Id<T>&id)
{
	return is >> id.name;
}

#endif /*ID_H*/
