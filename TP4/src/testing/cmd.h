// cmd.h : comandos del driver de casos de test.
// v1.3.0 -25nov2006- nueva implementación del decodificador
// v1.2.x -14oct2005- adaptado a propuestas de adrián
// v1.1.x -21oct2004- adaptado para 1er reuso
// v1.0.0 -24jun2004- versión publicable
// v0.1.1 -24may2004- creado

#ifndef CMD_H
#define CMD_H

#include <string>
#include "objectTable.h"
#include "result.h"
#include "usageException.h"
#include "badTypeException.h"
#include "errorTestException.h"
#include "invalidTestException.h"

namespace cmd
{
	// Ejecuta un comando con sus parámetros
	void exec(std::string cmdln, ObjectTable& symbolTable, Result& result) 
			throw (UsageException, BadTypeException, InvalidTestException, 
			ErrorTestException, std::ios_base::failure);
			
    // Parche feo para evitar escribir conj Galaxia al crear universo
    ConjGalaxia& idsEnGalaxias(const ConjIdGalaxia&);
}

#endif
