// exception.h : clase base de las excepciones.
// v1.0.0 -24jun2004- versi�n publicable
// v0.1.1 -20may2004- creado

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>
using namespace std;

class Exception
{
private:
    const string msg;
	// todo: unificar con el est�ndar.
public:
	Exception(string message = ""): msg(message) { }
    string message() const { return msg; }
};

#endif
