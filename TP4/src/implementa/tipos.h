#ifndef TIPOS_H
#define TIPOS_H

/* 
 * NOTA: es importante cuidar MUCHO el orden
 * en que se definen las inclusiones.
 */ 

// ---- TIPOS B�SICOS ----
// typedefs de tipos primitivos sueltos... 
#include "tiposBasicos.h"

// #includes de (tuplas de) tipos primitivos... 
typedef struct Agujero {
	Planeta unaPunta;
	Planeta otraPunta;
    friend istream& operator>>(istream&, Agujero&);
    friend ostream& operator<<(ostream&, const Agujero&);
    bool operator==(const Agujero& a) const{
    	return (!(*this<a) && !(a<*this));
    }
    
    bool operator<(const Agujero& a) const{
    	return ( (unaPunta + otraPunta ) < (a.unaPunta + a.otraPunta) );	
    }
    
};

// ---- COLECCIONES DE TIPOS B�SICOS ----
// algunas colecciones son tipos b�sicos,
// seg�n lo sea el elemento almacenado.
// #includes de contenedores para tipos b�sicos...
#include "lib/conjlog.h"
#include "lib/diccCte.h"
#include "lib/dicclog.h"
// #include "lib/secu.h"
#include "lib/arbolNarioSinDupl.h"

// typedefs de colecciones de tipos b�sicos...

#include "tostadora.h"
#include "galaxia.h"

// typedefs de colecciones de tipos no b�sicos...
typedef conjlog<Tostadora> ConjTostadora;
typedef conjlog<Galaxia> ConjGalaxia;

// Esto se necesita para no tener que escribir 
// galaxias completas cuando se crea un universo
// con el driver. El driver tomar� IDs de 
// galaxias. Si usan otro conjunto que no sea
// conjlog.h, revisen que el iterador anda bien
// con el m�todo cmd::idsEnGalaxias(ConjIdGalaxia)
#include "../testing/id.h"
typedef conjlog< Id<Galaxia> > ConjIdGalaxia;
typedef conjlog< Id<Galaxia> >::iterador IteradorConjIdGalaxia;

// IO para tipos
ostream& operator<<(ostream& os, const Agujero & output);
ostream& operator<<(ostream& os, const Tostadora& output);

istream& operator>>(istream& is, Agujero& input);
istream& operator>>(istream& is, Tostadora& input);

#endif // TIPOS_H
