#ifndef UNIVERSO_H
#define UNIVERSO_H

#include "tipos.h"


class Universo
{
    public:
        Universo(const ConjGalaxia& cg);
        void agTostadora(const Tostadora& t, const Planeta& p);
        void abrirAgujero(const Agujero& a);
        void ordenarViaje(const Tostadora& t, const Planeta& dest);

        bool esGalaxia(const Galaxia& g);
        bool esTostadora(const Tostadora& t);
        const Planeta& pos(const Tostadora& t);
        int viajes(const Tostadora& t);
        bool hayAgujero(const Agujero& a);
        int energia(const Agujero& a);
        bool hayCampeona();
        /** el resultado se devuelve por *copia*.  */
        const Tostadora campeona();
        //el destructor no estaba en el public original
        ~Universo();

        /** descomentar e implementar solo si se entregó el rtp3
         * Observar que también deben descomentar la invocación en
         * testing/cmd.cpp */
        // int visitas(const Planeta& p);
        // const Planeta& masVisitado();

    private:
        /* a completar por los alumnos */
        struct signTost {
        	Planeta pos;
        	Nat cantViajes;
        	Tostadora::Clase clase;
        };
        
       struct datoCampeona {
	       	string nombreT;
	       	Tostadora::Clase claseT;
	       	Nat viajesT;       
       };
       
       struct agujeroEncontrado {
	       	bool encontreAgujero;
	       	Agujero adg;
       };
        
        conjlog<Galaxia> *_galaxias;
        dicclog<string,signTost> *_tostadoras;
        diccCte<Agujero,Nat> *_agujeros;
        datoCampeona _campeona;
        bool _hayCampeona;
	Tostadora *campeonaPointer;
        
        Galaxia & _dameGalaxia( const Planeta &p, conjlog<Galaxia> & cg );
        conjlog<Planeta>& _planetasG( const Galaxia & g );
        agujeroEncontrado _dameUnAgujero(conjlog<Planeta>& cp, conjlog<Planeta>& cq);
        Agujero & _normalizarAgujero( const Agujero & a );
};

#endif
