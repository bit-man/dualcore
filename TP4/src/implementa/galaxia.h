#ifndef GALAXIA_H
#define GALAXIA_H

#include "tiposBasicos.h"
#include "lib/arbolNarioSinDupl.h"


class Galaxia {
    public:
        Galaxia(const Planeta& p);
        Galaxia(const Galaxia& g);
        // A pedido de conjlog y dicclog, este
        // constructor DEBE estar.
        Galaxia();
		/********
 		 * el destructor no estaba en el public original
 	     *******/
		~Galaxia();
		
        void agSat(const Planeta& p, const Planeta& s);

        const Planeta& centro();
        const ConjPlaneta& sats(const Planeta& p);
        bool esSat(const Planeta& p, const Planeta& q);

        // No es estrictamente necesaria esta operación.
        // Queda a criterio de los alumnos.
        // (si la usan, hay que desomentar en testing/cmd.cpp)
        // (busquen "/** Desomentar")
        // const ConjPlaneta planetas();

        // Necesarios para el driver
        bool operator==(const Galaxia& otra) const;
        bool operator!=(const Galaxia& otra) const;   //ESTÁ
        bool operator<(const Galaxia& otra) const;
        Galaxia& operator=(const Galaxia& otra);

        // Galaxia no necesita streaming en el driver
        // ostream& operator<<(ostream&, const Galaxia&) const;
        // istream& operator>>(istream&, Galaxia&);

    private:
		arbolNarioSinDupl<Planeta> *_centro;
};


#endif



