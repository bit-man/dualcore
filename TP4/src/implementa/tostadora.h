#ifndef TOSTADORA_H
#define TOSTADORA_H

#include "tipos.h"

using namespace std;

class Tostadora
{
    public:
        enum Clase { classic, enterprise };

        Tostadora() { }
        Tostadora(const string& nombre, Clase clase): nombre_(nombre), clase_(clase) { } // era así?

        const string& nombre() const { return nombre_; }
        const Clase& clase() const { return clase_; }

        friend istream& operator>>(istream&, Tostadora&);
        friend ostream& operator<<(ostream&, const Tostadora&);

        bool operator==(const Tostadora& otra) const
        {
            return this->nombre_ == otra.nombre_ 
                && this->clase_ == otra.clase_;
        }
        bool operator!=(const Tostadora& otra) const { return !(*this==otra); }

    private:
        string nombre_;
        Clase clase_;

        void setClase(const string& clase)
        {
            if (clase == "classic") clase_ = classic;
            else if (clase == "enterprise") clase_ = enterprise;
            else // Si llega abajo, es un valor invalido
                assert(false);
        }

        const string claseString() const
        {
            switch( clase() )
            {
                case classic:
                    return "classic";
                case enterprise:
                    return "enterprise";
            }
        }
};

#endif
