#ifndef TIPOSBASICOS_H
#define TIPOSBASICOS_H
#include<assert.h>

#include<string>
#include<iostream>

#include "lib/conjlog.h"

using namespace std;

typedef string Planeta;
typedef unsigned long int Nat;

typedef conjlog<Planeta> ConjPlaneta;

#endif
