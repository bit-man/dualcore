#include "tipos.h"

// IO para Agujero

ostream& operator<<(ostream& os, const Agujero& agujero) {
  return os << "< " << agujero.unaPunta << " " <<  agujero.otraPunta << " >";
}

istream& operator>>(istream& is, Agujero& agujero) {
  char nextChar;
  is >> nextChar;
  assert(nextChar == '<');

  is >> agujero.unaPunta;
  is >> agujero.otraPunta;

  is >> nextChar;
  assert(nextChar == '>');

  return is;
}


// IO para Tostadora

ostream& operator<<(ostream& os, const Tostadora& tostadora) {
  return os << "< " << tostadora.nombre() << " " << tostadora.claseString() << " >";
}

istream& operator>>(istream& is, Tostadora& tostadora) {
  char nextChar;
  is >> nextChar;
  assert(nextChar == '<');
  
  string clase;
  is >> tostadora.nombre_;
  is >> clase;
  tostadora.setClase(clase);

  is >> nextChar;
  assert(nextChar == '>');

  return is;
}
