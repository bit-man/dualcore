#include <stdlib.h>	
#include "galaxia.h"
#include "lib/conjlog.h"
#include "lib/arbolNarioSinDupl.h"
#include "tiposBasicos.h" 

using namespace std;

Galaxia::Galaxia(){
	_centro = NULL;
}

Galaxia::Galaxia(const Planeta& p) {
	conjlog<arbolNarioSinDupl<Planeta> > *conjA = new conjlog<arbolNarioSinDupl<Planeta> >();	 
	this->_centro = new arbolNarioSinDupl<Planeta>(p, *conjA);
	delete conjA;
}

/********
 * el destructor no estaba en el public original
 *******/
Galaxia::~Galaxia() {
	if ( _centro != NULL )	 
		delete _centro;
}

Galaxia::Galaxia(const Galaxia& g) {
	_centro = new arbolNarioSinDupl<Planeta>;
	*(_centro) = *(g._centro);
}


const ConjPlaneta& Galaxia::sats(const Planeta& p) { 
	return _centro->valoresHijos(p);
	
};

void Galaxia::agSat(const Planeta& p, const Planeta& s) {
	conjlog<arbolNarioSinDupl<Planeta> > *conjA = new conjlog<arbolNarioSinDupl<Planeta> >();
	arbolNarioSinDupl<Planeta> *aux = new arbolNarioSinDupl<Planeta>(s, *conjA);
	_centro->agregarHijo(p, *aux);
	delete conjA;
	delete aux;
};

bool Galaxia::esSat(const Planeta& p, const Planeta& q) {
    return this->sats(p).pertenece(q);
};

const Planeta& Galaxia::centro() {
    return this->_centro->raiz();
};


Galaxia &Galaxia::operator=(Galaxia const& otra) {

	if (this != &otra) {

		if (_centro != NULL )
	        delete _centro;

        _centro = new arbolNarioSinDupl<Planeta>;

        *(_centro) = *(otra._centro);

    };
    return *this;
}

bool Galaxia::operator==(const Galaxia& otra) const { 

	return ( *(_centro) == *(otra._centro) );		
	
}


bool Galaxia::operator!=(const Galaxia& g) const{
 	return !(*this == g);
}

bool Galaxia::operator<(const Galaxia& otra) const {
    return ( *_centro < *(otra._centro) );
}

