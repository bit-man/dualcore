
// Diccionario con complejidades logar'itmicas

#ifndef _DICCLOG_H
#define _DICCLOG_H

#include <cassert>
#include <iostream>
#include <stdlib.h>

#include "secu.h"

// asume un orden total C::operator<
// un C::operator==
// asume operadores de asignacion
//   C::operator=
//   V::operator=
template<class C, class V> class dicclog {
public:
	dicclog();
	~dicclog();
	dicclog(const dicclog<C, V>& d);
	dicclog<C, V>& operator=(const dicclog<C, V>& d);

	// operator== y operator!= requieren
	// ademas un V::operator==
	bool operator==(const dicclog<C, V>& d) const;
	bool operator!=(const dicclog<C, V>& d) const;

	dicclog<C, V>& definir(const C& clave, const V& valor);
	bool definido(const C& clave) const;
	V& obtener(const C& clave) const;
	dicclog<C, V>& borrar(const C& clave);

	unsigned int tamano() const;
	
	
private:
	void _destruir();
	void _copiar(const dicclog<C, V>& d);

	// implementado como un red-black tree
	// directamente sobre punteros
	struct nodo {
		bool es_rojo;
		struct nodo *padre;
		struct nodo *hijos[2];
		C *clave;
		V *valor;

#define IZQUIERDA	0
#define DERECHA		1
#define izq	hijos[IZQUIERDA]
#define der	hijos[DERECHA]

		// auxiliares para insercion
		struct nodo *abuelo();
		struct nodo *tio();
		void balancear_insercion_1();
		void balancear_insercion_2();
		void balancear_insercion_3();

		// auxiliares para borrado
		struct nodo *hermano();
		void balancear_borrado_1();
		void balancear_borrado_2();
		void balancear_borrado_3();

		// rotaciones
		void rotar(int direccion);
	};
	struct nodo *_raiz;
	unsigned int _cant_elems;

	void _borrar_nodo(struct nodo **p);
	void _borrar_nodo_con_hijo_nil(struct nodo **p);
	void _borrar_nodo_sin_hijo_nil(struct nodo **p);

public:
	// iterador constante
	class iterador_const {
	public:
		iterador_const();
		iterador_const(const dicclog<C, V>& d);
		iterador_const(const typename dicclog<C, V>::iterador_const& i);
		typename dicclog<C, V>::iterador_const&
			operator=(const typename dicclog<C, V>::iterador_const& i);
		bool operator==(const typename dicclog<C, V>::iterador_const& i);
		bool operator!=(const typename dicclog<C, V>::iterador_const& i);

		void comenzar();
		void siguiente();
		bool valido() const;
		const C& claveActual() const;
		V& valorActual() const;
		
		// borra el actual
        // invalida el iterador
        void borrarActual();
		
	protected:
		secu<typename dicclog<C, V>::nodo *> _pila;
		dicclog<C, V> *_diccionario;
	};

	// iterador mutable
	class iterador : public dicclog<C, V>::iterador_const {
	public:
		iterador();
		iterador(dicclog<C, V>& d);
		iterador(const typename dicclog<C, V>::iterador_const& i);

		// borra el actual
		// *invalida el iterador*
		void borrarActual();
	};

	// iterador inorder
	class iterador_inorder {
	public:
		iterador_inorder();
		iterador_inorder(const dicclog<C, V>& d);
		iterador_inorder(const dicclog<C, V>& d, const C& c);
		iterador_inorder(const typename dicclog<C, V>::iterador_inorder& i);
		typename dicclog<C, V>::iterador_inorder&
			operator=(const typename dicclog<C, V>::iterador_inorder& i);
		bool operator==(const typename dicclog<C, V>::iterador_inorder& i);
		bool operator!=(const typename dicclog<C, V>::iterador_inorder& i);

		void comenzar();
		void comenzarDesde(const C& clave);
		void siguiente();
		bool valido() const;
		const C& claveActual() const;
		V& valorActual() const;
		
		// borra el actual
        // invalida el iterador
        void borrarActual();
		
	private:
		void _bajar_izq();
	protected:
		// se guarda una lista de bool,
		// cada uno de los cuales corresponde
		// a un nodo en la rama que estoy visitando.
		//   false => el nodo todavia no fue visitado
		//            (cuando suba, tengo que mirarlo y
		//            despues visitar su hijo derecho)
		//   true  => el nodo ya fue visitado
		//            (cuando suba, tengo que seguir subiendo)
		secu<bool> _pila;
		typename dicclog<C, V>::nodo *_actual;
		dicclog<C, V> *_diccionario;
	};
};

template <class C, class V>
typename dicclog<C, V>::nodo *dicclog<C, V>::nodo::abuelo() {
	assert(padre != NULL);
	return padre->padre;
}

template <class C, class V>
typename dicclog<C, V>::nodo *dicclog<C, V>::nodo::tio() {
	assert(padre != NULL);

	typename dicclog<C, V>::nodo *a = abuelo();
	assert(a != NULL);

	if (padre == a->izq)
		return a->der;
	else
		return a->izq;
}

template <class C, class V>
typename dicclog<C, V>::nodo *dicclog<C, V>::nodo::hermano() {
	assert(padre != NULL);
	if (this == padre->izq)
		return padre->der;
	else
		return padre->izq;
}

template <class C, class V>
void dicclog<C, V>::nodo::balancear_insercion_1() {
	if (padre == NULL)
		es_rojo = false;
	else if (padre->es_rojo)
		balancear_insercion_2();
}

template <class C, class V>
void dicclog<C, V>::nodo::balancear_insercion_2() {
	typename dicclog<C, V>::nodo *t = tio();
	typename dicclog<C, V>::nodo *a = abuelo();
	if (t != NULL && t->es_rojo) {
		padre->es_rojo = false;
		t->es_rojo = false;
		a->es_rojo = true;
		a->balancear_insercion_1();
	} else {
		if (this == padre->der && padre == a->izq)
			padre->rotar(IZQUIERDA);
		else if (this == padre->izq && padre == a->der)
			padre->rotar(DERECHA);
		balancear_insercion_3();
	}
}

template <class C, class V>
void dicclog<C, V>::nodo::balancear_insercion_3() {
	typename dicclog<C, V>::nodo *a = abuelo();

	padre->es_rojo = false;
	a->es_rojo = true;
	if (this == padre->izq && padre == a->izq)
		a->rotar(DERECHA);
	else {
		assert(this == padre->der && padre == a->der);
		a->rotar(IZQUIERDA);
	}
}

#define ES_ROJO(X)	(((X) != NULL) && (X)->es_rojo)
#define ES_NEGRO(X)	(!ES_ROJO(X))

template <class C, class V>
void dicclog<C, V>::nodo::balancear_borrado_1() {
	if (padre == NULL)
		return;
	else {
		typename dicclog<C, V>::nodo *h = hermano();

		if (ES_ROJO(h)) {
			padre->es_rojo = true;
			h->es_rojo = false;
			if (this == padre->izq) {
				padre->rotar(IZQUIERDA);
			} else {
				padre->rotar(DERECHA);
			}
			balancear_borrado_2();
		}
	}
}

template <class C, class V>
void dicclog<C, V>::nodo::balancear_borrado_2() {
	typename dicclog<C, V>::nodo *h = hermano();

	if (h == NULL) return;

	if (!padre->es_rojo
		&& !h->es_rojo
		&& ES_NEGRO(h->izq)
		&& ES_NEGRO(h->der)) {
		h->es_rojo = true;
		padre->balancear_borrado_1();
	} else if (padre->es_rojo
			&& !h->es_rojo
			&& ES_NEGRO(h->izq)
			&& ES_NEGRO(h->der)) {
		h->es_rojo = true;
		padre->es_rojo = false;
	} else {
		if (this == padre->izq
				&& !h->es_rojo
				&& ES_ROJO(h->izq)
				&& ES_NEGRO(h->der)) {
			h->es_rojo = true;
			h->izq->es_rojo = false;
			h->rotar(DERECHA);
		} else if (this == padre->der
					&& !h->es_rojo
					&& ES_NEGRO(h->izq)
					&& ES_ROJO(h->der)) {
			h->es_rojo = true;
			h->der->es_rojo = true;
			h->rotar(IZQUIERDA);
		}
		balancear_borrado_3();
	}
}

template <class C, class V>
void dicclog<C, V>::nodo::balancear_borrado_3() {
	typename dicclog<C, V>::nodo *h = hermano();

	h->es_rojo = padre->es_rojo;
	padre->es_rojo = false;
	if (this == padre->izq) {
		h->der->es_rojo = false;
		padre->rotar(IZQUIERDA);
	} else {
		h->izq->es_rojo = false;
		padre->rotar(DERECHA);
	}
}

template <class C, class V>
void dicclog<C, V>::nodo::rotar(int direccion) {
	typename dicclog<C, V>::nodo *tmp;

	bool _es_rojo;
	typename dicclog<C, V>::nodo *_hijo;
	C *_clave;
	V *_valor;

	tmp = hijos[!direccion];
	assert(tmp != NULL);

	_es_rojo = es_rojo;
	_hijo = hijos[direccion];
	_clave = clave;
	_valor = valor;

	es_rojo = tmp->es_rojo;
	clave = tmp->clave;
	valor = tmp->valor;
	hijos[direccion] = tmp;
	hijos[!direccion] = tmp->hijos[!direccion];
	if (hijos[!direccion] != NULL) hijos[!direccion]->padre = this;

	tmp->es_rojo = _es_rojo;
	tmp->clave = _clave;
	tmp->valor = _valor;
	tmp->hijos[!direccion] = tmp->hijos[direccion];
	tmp->hijos[direccion] = _hijo;
	if (_hijo != NULL) _hijo->padre = tmp;
}

template <class C, class V>
dicclog<C, V>::dicclog()
{
	_raiz = NULL;
	_cant_elems = 0;
}

template <class C, class V>
dicclog<C, V>::~dicclog()
{
	_destruir();
}

template <class C, class V>
dicclog<C, V>::dicclog(const dicclog<C, V>& d)
{
	_copiar(d);
}

template <class C, class V>
dicclog<C, V>& dicclog<C, V>::operator=(const dicclog<C, V>& d)
{
	if (this != &d) {
		_destruir();
		_copiar(d);
	}
	return *this;
}

template <class C, class V>
void dicclog<C, V>::_destruir()
{
	secu<typename dicclog<C, V>::nodo *> p;
	typename dicclog<C, V>::nodo *n;

	if (_raiz != NULL) p.insertar(_raiz);
	
	while (!p.esVacia()) {
		n = p.primero();
		p.fin();

		if (n->izq != NULL) p.insertar(n->izq);
		if (n->der != NULL) p.insertar(n->der);

		delete n->clave;
		delete n->valor;
		delete n;
	}
}

// ojo, est'a teniendo costo O(n * log(n))
// porque recorre el origen y va definiendo
// (clave, valor) en el destino
//
// se podria hacer O(n) copiando la estructura del arbol
//
// [ modulo costos de comparar claves y copiar claves/valores ]
template <class C, class V>
void dicclog<C, V>::_copiar(const dicclog<C, V>& d)
{
	_raiz = NULL;
	_cant_elems = 0;

	typename dicclog<C, V>::iterador_const i(d);

	while (i.valido()) {
		definir(i.claveActual(), i.valorActual());
		i.siguiente();
	}
}

template <class C, class V>
bool dicclog<C, V>::operator==(const dicclog<C, V>& d) const
{
	if (_cant_elems != d._cant_elems)
		return false;

	typename dicclog<C, V>::iterador_const i(d);
	while (i.valido()) {
		if (!definido(i.claveActual()))
			return false;
		if (!(obtener(i.claveActual()) == i.valorActual()))
			return false;
		i.siguiente();
	}
	return true;
}

template <class C, class V>
bool dicclog<C, V>::operator!=(const dicclog<C, V>& d) const
{
	return !(*this == d);
}

template <class C, class V>
dicclog<C, V>& dicclog<C, V>::definir(const C& clave, const V& valor)
{
	typename dicclog<C, V>::nodo **p = &_raiz;
	typename dicclog<C, V>::nodo *padre = NULL;

	while (*p != NULL) {
		padre = *p;
		if (*(*p)->clave == clave) {
			// ya existia -> lo reemplazo
			// usa el operador de asignacion
			*(*p)->valor = valor;
			return *this;
		} else if (clave < *(*p)->clave)
			p = &(*p)->izq;
		else {
			assert(*(*p)->clave < clave);
			p = &(*p)->der;
		}
	}

	typename dicclog<C, V>::nodo *n = new typename dicclog<C, V>::nodo();
	*p = n;

	++_cant_elems;
	n->es_rojo = true;
	n->izq = NULL;
	n->der = NULL;
	n->padre = padre;
	n->clave = new C;
	*n->clave = clave;
	n->valor = new V;
	*n->valor = valor;

	n->balancear_insercion_1();
	return *this;
}

template <class C, class V>
bool dicclog<C, V>::definido(const C& clave) const
{
	typename dicclog<C, V>::nodo *p = _raiz;

	while (p != NULL) {
		if (*p->clave == clave)
			return true;
		else if (clave < *p->clave)
			p = p->izq;
		else {
			assert(*p->clave < clave);
			p = p->der;
		}
	}
	return false;
}

template <class C, class V>
V& dicclog<C, V>::obtener(const C& clave) const
{
	typename dicclog<C, V>::nodo *p = _raiz;

	while (p != NULL) {
		if (*p->clave == clave)
			return *p->valor;
		else if (clave < *p->clave)
			p = p->izq;
		else {
			assert(*p->clave < clave);
			p = p->der;
		}
	}
	assert(false);
}

template <class C, class V>
dicclog<C, V>& dicclog<C, V>::borrar(const C& clave)
{
	typename dicclog<C, V>::nodo **p = &_raiz;

	while (*p != NULL) {
		if (*(*p)->clave == clave) {
			_borrar_nodo(p);
			return *this;
		} else if (clave < *(*p)->clave)
			p = &(*p)->izq;
		else {
			assert(*(*p)->clave < clave);
			p = &(*p)->der;
		}
	}
	// no lo encontre
	return *this;
}

template <class C, class V>
unsigned int dicclog<C, V>::tamano() const {
	return _cant_elems;
}

template <class C, class V>
void dicclog<C, V>::_borrar_nodo(typename dicclog<C, V>::nodo **p) {
	delete (*p)->clave;
	delete (*p)->valor;
	if ((*p)->izq == NULL || (*p)->der == NULL)
		// algun hijo es Nil
		_borrar_nodo_con_hijo_nil(p);
	else
		// ambos hijos NO son Nil
		_borrar_nodo_sin_hijo_nil(p);
	--_cant_elems;
}

// * PRE: alguno de los hijos es Nil
// * NO borra la clave ni el valor
template <class C, class V>
void dicclog<C, V>::_borrar_nodo_con_hijo_nil(typename dicclog<C, V>::nodo **p)
{
	assert((*p)->izq == NULL || (*p)->der == NULL);
	typename dicclog<C, V>::nodo *b = *p;
	typename dicclog<C, V>::nodo *hijo;

	hijo = (b->izq != NULL) ? b->izq : b->der;
	*p = hijo;
	if (hijo != NULL) {
		hijo->padre = b->padre;

		// tres casos:
		//   b->es_rojo
		//      => no hay problema
		//   !b->es_rojo && hijo->es_rojo
		//      => pinto al hijo de negro
		//   !b->es_rojo && !hijo->es_rojo
		//      => hay que balancear
		if (!b->es_rojo) {
			if (hijo->es_rojo)
				hijo->es_rojo = false;
			else 
				hijo->balancear_borrado_1();
		}
	}
	delete b;
}

// * PRE: ambos hijos NO son Nil
// * NO borra la clave ni el valor
template <class C, class V>
void dicclog<C, V>::_borrar_nodo_sin_hijo_nil(typename dicclog<C, V>::nodo **p)
{
	assert((*p)->izq != NULL && (*p)->der != NULL);

	// busco el mayor de los menores
	typename dicclog<C, V>::nodo **q = &(*p)->izq;
	while ((*q)->der != NULL) {
		q = &(*q)->der;
	}

	// muevo el contenido del mayor de los menores al
	// nodo que queria borrar
	(*p)->clave = (*q)->clave;
	(*p)->valor = (*q)->valor;

	// borro el mayor de los menores
	_borrar_nodo_con_hijo_nil(q);
}

// iterador constante

template <class C, class V>
dicclog<C, V>::iterador_const::iterador_const() {
	_diccionario = NULL;
}

template <class C, class V>
dicclog<C, V>::iterador_const::iterador_const(const dicclog<C, V>& d) {
	_diccionario = const_cast<dicclog<C, V> *>(&d);
	if (d._raiz != NULL) {
		_pila.insertar(d._raiz);
	}
}

template <class C, class V>
dicclog<C, V>::iterador_const::iterador_const(
		const typename dicclog<C, V>::iterador_const& i) {
	_pila = i._pila;
	_diccionario = i._diccionario;
}

template <class C, class V>
typename dicclog<C, V>::iterador_const&
dicclog<C, V>::iterador_const::operator=(
		const typename dicclog<C, V>::iterador_const& i) {
	_pila = i._pila;
	_diccionario = i._diccionario;
	return *this;
}

template <class C, class V>
bool dicclog<C, V>::iterador_const::operator==(
		const typename dicclog<C, V>::iterador_const& i) {
	return _pila == i._pila && _diccionario == i._diccionario;
}

template <class C, class V>
bool dicclog<C, V>::iterador_const::operator!=(
		const typename dicclog<C, V>::iterador_const& i) {
	return !(*this == i);
}

template <class C, class V>
void dicclog<C, V>::iterador_const::comenzar() {
	assert(_diccionario != NULL);
	_pila = secu<typename dicclog<C, V>::nodo *>();
	if (_diccionario->_raiz != NULL) {
		_pila.insertar(_diccionario->_raiz);
	} else {
		_diccionario = NULL;
	}
}

template <class C, class V>
bool dicclog<C, V>::iterador_const::valido() const {
	return _diccionario != NULL && !_pila.esVacia();
}

template <class C, class V>
void dicclog<C, V>::iterador_const::siguiente() {
	assert(valido());

	typename dicclog<C, V>::nodo &n = *_pila.primero();
	_pila.fin();
	if (n.izq != NULL) _pila.insertar(n.izq);
	if (n.der != NULL) _pila.insertar(n.der);
}

template <class C, class V>
const C& dicclog<C, V>::iterador_const::claveActual() const {
	assert(valido());

	typename dicclog<C, V>::nodo &n = *_pila.primero();
	return *n.clave;
}

template <class C, class V>
V& dicclog<C, V>::iterador_const::valorActual() const {
	assert(valido());

	typename dicclog<C, V>::nodo &n = *_pila.primero();
	return *n.valor;
}

// iterador mutable

#define _pila		(this->_pila)
#define _diccionario	(this->_diccionario)
#define valido		(this->valido)

template <class C, class V>
dicclog<C, V>::iterador::iterador() { }

template <class C, class V>
dicclog<C, V>::iterador::iterador(dicclog<C, V>& d) :
	dicclog<C, V>::iterador_const::iterador_const(d)
{
}

template <class C, class V>
dicclog<C, V>::iterador::iterador(const typename dicclog<C, V>::iterador_const& i) :
	dicclog<C, V>::iterador_const::iterador_const(i)
{
}

template <class C, class V>
void dicclog<C, V>::iterador::borrarActual() {
	assert(valido());

	typename dicclog<C, V>::nodo &n = *_pila.primero();

	// esta implementacion aprovecha el hecho de que
	// estoy parado en el nodo que quiero borrar.
	//
	// seria O(1) porque todo lo que hago es una
	// cantidad constante de rotaciones.
	typename dicclog<C, V>::nodo **posicion;
	if (n.padre == NULL)
		posicion = &_diccionario->_raiz;
	else if (n.padre->izq == &n)
		posicion = &n.padre->izq;
	else {
		assert(n.padre->der == &n);
		posicion = &n.padre->der;
	}
	_diccionario->_borrar_nodo(posicion);

	// invalida el iterador
	_diccionario = NULL;
}

#undef valido
#undef _pila
#undef _diccionario

// iterador inorder

template <class C, class V>
dicclog<C, V>::iterador_inorder::iterador_inorder() {
	_diccionario = NULL;
}

template <class C, class V>
dicclog<C, V>::iterador_inorder::iterador_inorder(const dicclog<C, V>& d) {
	_diccionario = const_cast<dicclog<C, V> *>(&d);
	comenzar();
}

template <class C, class V>
dicclog<C, V>::iterador_inorder::iterador_inorder(const dicclog<C, V>& d, const C& c) {
	_diccionario = const_cast<dicclog<C, V> *>(&d);
	comenzarDesde(c);
}

template <class C, class V>
dicclog<C, V>::iterador_inorder::iterador_inorder(
		const typename dicclog<C, V>::iterador_inorder& i) {
	_diccionario = i._diccionario;
	_actual = i._actual;
	_pila = i._pila;
}

template <class C, class V>
typename dicclog<C, V>::iterador_inorder&
dicclog<C, V>::iterador_inorder::operator=(
		const typename dicclog<C, V>::iterador_inorder& i) {
	_diccionario = i._diccionario;
	_actual = i._actual;
	_pila = i._pila;
	return *this;
}

template <class C, class V>
bool dicclog<C, V>::iterador_inorder::operator==(
		const typename dicclog<C, V>::iterador_inorder& i) {
	return _diccionario == i._diccionario
		&& _actual == i._actual
		&& _pila == i._pila;
}

template <class C, class V>
bool dicclog<C, V>::iterador_inorder::operator!=(
		const typename dicclog<C, V>::iterador_inorder& i) {
	return !(*this == i);
}

template <class C, class V>
void dicclog<C, V>::iterador_inorder::comenzar() {
	assert(_diccionario != NULL);

	_pila = secu<bool>();
	if (_diccionario->_raiz != NULL) {
		_actual = _diccionario->_raiz;
		_bajar_izq();
	} else {
		_actual = NULL;
	}
}

template <class C, class V>
void dicclog<C, V>::iterador_inorder::comenzarDesde(const C& clave) {
	assert(_diccionario != NULL);

	_pila = secu<bool>();
	if (_diccionario->_raiz != NULL) {
		_actual = _diccionario->_raiz;
		// bajo hasta el nodo donde esta la clave
		// (o hasta el mas cercano)
		while (true) {
			if (clave == *_actual->clave) {
				_pila.insertar(false);
				break;
			} else if (clave < *_actual->clave) {
				_pila.insertar(false);
				if (_actual->izq == NULL) break;
				_actual = _actual->izq;
			} else {
				assert(*_actual->clave < clave);
				_pila.insertar(true);
				if (_actual->der == NULL) break;
				_actual = _actual->der;
			}
		}
		// si quede parado en un nodo mayor que la
		// clave que busco, tengo que subir
		while (_actual != NULL && _pila.primero() == true) {
			_pila.fin();
			_actual = _actual->padre;
		}
	} else {
		_actual = NULL;
	}
}

template <class C, class V>
void dicclog<C, V>::iterador_inorder::_bajar_izq() {
	assert(_diccionario != NULL && _actual != NULL);
	// baja el puntero yendo por los hijos izquierdos
	// todo lo que sea posible
	_pila.insertar(false);
	while (_actual->izq != NULL) {
		_pila.insertar(false);
		_actual = _actual->izq;
	}
}

template <class C, class V>
bool dicclog<C, V>::iterador_inorder::valido() const {
	return _diccionario != NULL && _actual != NULL;
}

template <class C, class V>
void dicclog<C, V>::iterador_inorder::siguiente() {
	assert(valido());

	if (_pila.esVacia())
		_actual = NULL;
	else if (_pila.primero() == false) {
		_pila.fin();
		if (_actual->der != NULL) {
			// me voy al hijo derecho
			_pila.insertar(true);
			_actual = _actual->der;
			_bajar_izq();
		} else {
			// subo hasta que encuentro un nodo no visitado
			_actual = _actual->padre;
			while (_actual != NULL && _pila.primero() == true) {
				_pila.fin();
				_actual = _actual->padre;
			}
		}
	} else if (_pila.primero() == true) {
		assert(false);
	}
}

template <class C, class V>
const C& dicclog<C, V>::iterador_inorder::claveActual() const {
	assert(valido());

	return *(*_actual).clave;
}

template <class C, class V>
V& dicclog<C, V>::iterador_inorder::valorActual() const {
	assert(valido());

	return *(*_actual).valor;
}

template <class C, class V>
void dicclog<C, V>::iterador_inorder::borrarActual() {
       assert(valido());

       typename dicclog<C, V>::nodo &n = *_actual;

       // esta implementacion aprovecha el hecho de que
       // estoy parado en el nodo que quiero borrar.
       //
       // seria O(1) porque todo lo que hago es una
       // cantidad constante de rotaciones.
       typename dicclog<C, V>::nodo **posicion;
       if (n.padre == NULL)
               posicion = &_diccionario->_raiz;
       else if (n.padre->izq == &n)
               posicion = &n.padre->izq;
       else {
               assert(n.padre->der == &n);
               posicion = &n.padre->der;
       }
       _diccionario->_borrar_nodo(posicion);

       // invalida el iterador
       _diccionario = NULL;
}




#undef IZQUIERDA
#undef DERECHA
#undef izq
#undef der
#undef ES_ROJO
#undef ES_NEGRO

#endif /*_DICCLOG_H*/

