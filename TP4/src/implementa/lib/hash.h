
#ifndef _MYHASH_H_
#define _MYHASH_H_

#include <string>
#include "../tipos.h"

unsigned int hash( unsigned int colision );
unsigned int hash( unsigned int colision, int v );
unsigned int hash( unsigned int colision, std::string valor );
unsigned int hash( std::string v, unsigned int inicio, unsigned int fin ); // hash parcial de un string, sin colision

unsigned int hash(unsigned int, const Agujero);


#endif
