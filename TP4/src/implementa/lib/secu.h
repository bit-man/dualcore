
#ifndef _SECU_H
#define _SECU_H

#include <cassert>
#include <iostream>
#include <stdlib.h>

template<class T> class secu {
public:
	secu();
	~secu();

	secu(const secu<T>& s);

	secu<T>& operator=(const secu<T>& s);

	// operator== y operator!= requieren T::operator==
	bool operator==(const secu<T>& s) const;
	bool operator!=(const secu<T>& s) const;

	secu<T>& insertar(const T& e);
	secu<T>& insertarAlFinal(const T& e);
	// insertarOrdenado requiere T::operator< y T::operator==
	secu<T>& insertarOrdenado(const T& e);

	// primero y ultimo devuelven referencias NO const,
	// posiblemente asignables
	T& primero() const;
	T& ultimo() const;

	secu<T>& comienzo();
	secu<T>& fin();

	bool esVacia() const;
	unsigned int longitud() const;

	// despues de concatenar, s2 pasa a estar vacia
	secu<T>& concatenar(secu<T>& s2);

	// devuelve si la lista esta ordenada crecientemente
	// (no necesariamente estrictamente)
	// asume T::operator<
	bool ordenada() const;
	// despues de hacer merge, s2 pasa a estar vacia
	// asume T::operator< y T::operator==
	// y que tanto *this como s2 estan ordenadas
	secu<T>& merge(secu<T>& s2);
	// asume T::operator< y T::operator==
	secu<T>& ordenar();
	
	// borrar requiere T::operator==
	secu<T>& borrar(const T& x);
	// esta y cantApariciones requieren T::operator==
	bool esta(const T& x) const;
	unsigned int cantApariciones(const T& x) const;

	secu<T>& vaciar();

	// requiere operator<< para T
	template<class T1> friend std::ostream&
		operator<<(std::ostream& salida, const secu<T1>& s);
private:
	// implementada como una lista doblemente enlazada
	struct nodo {
		T valor;
		struct nodo *siguiente;
		struct nodo *anterior;
	};
	struct nodo *_primero;
	struct nodo *_ultimo;
	unsigned int _longitud;

	void _copiar(const secu<T>& s);
	void _destruir();
public:
	// iterador constante
	class iterador_const {
	public:
		iterador_const();
		iterador_const(const secu<T>& s);
		iterador_const(const typename secu<T>::iterador_const& i);
		typename secu<T>::iterador_const& operator=(const typename secu<T>::iterador_const& i);
		bool operator==(const typename secu<T>::iterador_const& i);
		bool operator!=(const typename secu<T>::iterador_const& i);

		bool valido() const;
		void irAlPrimero();
		void irAlUltimo();
		void irAlEnesimo(unsigned int n); // cuenta desde 0
		void anterior();
		void siguiente();
		T& actual() const;
	protected:
		secu<T> *_suby;
		typename secu<T>::nodo *_pos;
	};

	// iterador mutable
	class iterador : public secu<T>::iterador_const {
	public:
		iterador();
		iterador(secu<T>& s);
		iterador(const typename secu<T>::iterador_const& i);

		void insertarAntes(const T& e);
		void insertarDespues(const T& e);
		
		// "corta" la secuencia
		// desde donde esta el iterador en adelante
		// (excluyendo el elemento actual)
		// y devolviendo el sufijo en s2
		void split(secu<T>& s2);

		// borra el actual y avanza al siguiente
		// si no hay siguiente, se invalida el iterador
		void borrarActual();
	};
};

template<class T>
secu<T>::secu()
{
	_primero = NULL;
	_ultimo = NULL;
	_longitud = 0;
}

template<class T>
secu<T>::secu(const secu<T>& s)
{
	_copiar(s);
}

template<class T>
secu<T>::~secu()
{
	_destruir();
}

template<class T>
void secu<T>::_copiar(const secu<T>& s)
{
	typename secu<T>::nodo *p, *q;
	typename secu<T>::nodo **actual;

	_longitud = s._longitud;
	_ultimo = NULL;
	p = s._primero;
	actual = &_primero;
	while (p != NULL) {
		 q = new typename secu<T>::nodo;
		 q->valor = p->valor;
		 q->anterior = _ultimo;
		 *actual = _ultimo = q;
		 actual = &q->siguiente;
		 p = p->siguiente;
	}
	*actual = NULL;
}

template<class T>
void secu<T>::_destruir()
{
	typename secu<T>::nodo *p;

	while (_primero != NULL) {
		p = _primero;
		_primero = _primero->siguiente;
		delete p;
	}
}

template<class T>
bool secu<T>::operator==(const secu<T>& s) const
{
	typename secu<T>::nodo *p1, *p2;

	for (p1 = _primero, p2 = s._primero;
		p1 != NULL && p2 != NULL;
		p1 = p1->siguiente, p2 = p2->siguiente) {

		if (!(p1->valor == p2->valor))
			return false;
	}
	return p1 == NULL && p2 == NULL;
}

template<class T>
bool secu<T>::operator!=(const secu<T>& s) const
{
	return !(*this == s);
}

template<class T>
secu<T>& secu<T>::operator=(const secu<T>& s)
{
	if (this != &s) {
		_destruir();
		_copiar(s);
	}
	return *this;
}

template<class T>
secu<T>& secu<T>::insertar(const T& e)
{
	typename secu<T>::nodo *nuevo = new typename secu<T>::nodo;

	nuevo->valor = e;	// copia
	nuevo->siguiente = _primero;
	nuevo->anterior = NULL;
	if (_primero == NULL)
		_ultimo = nuevo;
	else
		_primero->anterior = nuevo;
	_primero = nuevo;
	++_longitud;
	return *this;
}

template<class T>
secu<T>& secu<T>::insertarOrdenado(const T& e) {
	typename secu<T>::nodo *nuevo = new typename secu<T>::nodo;
	nuevo->valor = e;	// copia

	typename secu<T>::nodo **actual;

	actual = &_primero;
	while (*actual != NULL && (*actual)->valor < e)
		actual = &(*actual)->siguiente;

	if (*actual != NULL) {
		nuevo->anterior = (*actual)->anterior;
		(*actual)->anterior = nuevo;
	} else {
		nuevo->anterior = _ultimo;
		_ultimo = nuevo;
	}

	nuevo->siguiente = *actual;
	*actual = nuevo;

	++_longitud;
	return *this;
}

template<class T>
secu<T>& secu<T>::insertarAlFinal(const T& e)
{
	typename secu<T>::nodo *nuevo = new typename secu<T>::nodo;

	nuevo->valor = e;	// copia
	nuevo->siguiente = NULL;
	nuevo->anterior = _ultimo;
	if (_ultimo == NULL)
		_primero = nuevo;
	else
		_ultimo->siguiente = nuevo;
	_ultimo = nuevo;
	++_longitud;
	return *this;
}

template<class T>
T& secu<T>::primero() const
{
	assert(!esVacia());
	return _primero->valor;
}

template<class T>
T& secu<T>::ultimo() const
{
	assert(!esVacia());
	return _ultimo->valor;
}

template<class T>
secu<T>& secu<T>::comienzo()
{
	typename secu<T>::nodo *anteultimo;

	assert(!esVacia());
	--_longitud;
	anteultimo = _ultimo->anterior;
	delete _ultimo;
	_ultimo = anteultimo;
	if (_ultimo == NULL)
		_primero = NULL;
	else
		_ultimo->siguiente = NULL;
	return *this;
}

template<class T>
secu<T>& secu<T>::fin()
{
	typename secu<T>::nodo *segundo;

	assert(!esVacia());
	--_longitud;
	segundo = _primero->siguiente;
	delete _primero;
	_primero = segundo;
	if (_primero == NULL)
		_ultimo = NULL;
	else
		_primero->anterior = NULL;
	return *this;
}

template<class T>
bool secu<T>::esVacia() const
{
	return _primero == NULL;
}

template<class T>
unsigned int secu<T>::longitud() const
{
	return _longitud;
}

template<class T>
secu<T>& secu<T>::concatenar(secu<T>& s2)
{
	assert(this != &s2);

	if (esVacia())
		_primero = s2._primero;
	else
		_ultimo->siguiente = s2._primero;
	if (!s2.esVacia()) {
		s2._primero->anterior = _ultimo;
		_ultimo = s2._ultimo;
	}
	_longitud += s2._longitud;

	// s2 pasa a estar vacia
	s2._primero = NULL;
	s2._ultimo = NULL;
	s2._longitud = 0;

	return *this;
}

// asumimos T::operator<
// OJO: no queremos asumir T::operator<=
template<class T>
bool secu<T>::ordenada() const
{
	typename secu<T>::nodo *p;
	for (p = _primero; p != NULL && p->siguiente != NULL;
			p = p->siguiente) {
		if (p->siguiente->valor < p->valor)
			return false;
	}
	return true;
}

template<class T>
secu<T>& secu<T>::merge(secu<T>& s2)
{
	assert(this != &s2);
	assert(ordenada());
	assert(s2.ordenada());

	typename secu<T>::nodo *p, *q, *ant;
	typename secu<T>::nodo **actual;

	if (_primero == NULL && s2._primero == NULL)
		// el merge deja todo igual
		return *this;

	ant = NULL;
	actual = &_primero;
	for (p = _primero, q = s2._primero; p != NULL && q != NULL;) {
		if (q->valor < p->valor) {
			// avanzo q
			q->anterior = ant;
			*actual = ant = q;
			actual = &q->siguiente;
			q = q->siguiente;
		} else {
			// avanzo p
			p->anterior = ant;
			*actual = ant = p;
			actual = &p->siguiente;
			p = p->siguiente;
		}
	}

	if (p != NULL) {
		p->anterior = ant;
		*actual = p;
	} else {
		assert(q != NULL);
		q->anterior = ant;
		*actual = q;
		_ultimo = s2._ultimo;
	}

	_longitud += s2._longitud;

	// s2 pasa a estar vacia
	s2._primero = NULL;
	s2._ultimo = NULL;
	s2._longitud = 0;

	return *this;
}

template<class T>
secu<T>& secu<T>::ordenar()
{
	// mergesort
	if (_longitud <= 1) return *this;
	secu<T> s2;
	typename secu<T>::iterador it(*this);
	it.irAlEnesimo(_longitud / 2 - 1);
	it.split(s2);
	ordenar();
	s2.ordenar();
	return merge(s2);
}

template<class T>
secu<T>& secu<T>::borrar(const T& x)
{
	typename secu<T>::nodo *p, *prox;
	typename secu<T>::nodo **actual;

	_ultimo = NULL;
	actual = &_primero;
	for (p = _primero; p != NULL; p = prox) {
		prox = p->siguiente;
		if (p->valor == x) {
			delete p;
			--_longitud;
		} else {
			p->anterior = _ultimo;
			*actual = _ultimo = p;
			actual = &p->siguiente;
		}
	}
	*actual = NULL;
	return *this;
}

template<class T>
bool secu<T>::esta(const T& x) const
{
	typename secu<T>::nodo *p;
	for (p = _primero; p != NULL; p = p->siguiente) {
		if (p->valor == x)
			return true;
	}
	return false;
}

template<class T>
unsigned int secu<T>::cantApariciones(const T& x) const
{
	unsigned int r = 0;
	typename secu<T>::nodo *p;
	for (p = _primero; p != NULL; p = p->siguiente) {
		if (p->valor == x)
			++r;
	}
	return r;
}

template<class T>
secu<T>& secu<T>::vaciar()
{
	_destruir();
	_primero = NULL;
	_ultimo = NULL;
	_longitud = 0;
	return *this;
}

/* iterador */

template<class T>
secu<T>::iterador_const::iterador_const()
{
	_suby = NULL;
	_pos = NULL;
}

template<class T>
secu<T>::iterador_const::iterador_const(const secu<T>& s)
{
	_suby = const_cast<secu<T> *>(&s);
	irAlPrimero();
}

template <class T>
secu<T>::iterador_const::iterador_const(const typename secu<T>::iterador_const& i)
{
	_suby = i._suby;
	_pos = i._pos;
}

template <class T>
bool secu<T>::iterador_const::operator==(const typename secu<T>::iterador_const& i)
{
	return _suby == i._suby && _pos == i._pos;
}

template <class T>
bool secu<T>::iterador_const::operator!=(const typename secu<T>::iterador_const& i)
{
	return !(*this == i);
}

template <class T>
typename secu<T>::iterador_const&
secu<T>::iterador_const::operator=(const typename secu<T>::iterador_const& i)
{
	_suby = i._suby;
	_pos = i._pos;
	return *this;
}

template <class T>
bool secu<T>::iterador_const::valido() const
{
	return _suby != NULL && _pos != NULL;
}

template<class T>
void secu<T>::iterador_const::irAlPrimero()
{
	assert(_suby != NULL);
	_pos = _suby->_primero;
}

template<class T>
void secu<T>::iterador_const::irAlUltimo()
{
	assert(_suby != NULL);
	_pos = _suby->_ultimo;
}

template<class T>
void secu<T>::iterador_const::irAlEnesimo(unsigned int n)
{
	unsigned int i = 0;
	assert(_suby != NULL);
	for (_pos = _suby->_primero;
		_pos != NULL && i < n;
		_pos = _pos->siguiente, ++i) { /* vacio */ }
}

template<class T>
void secu<T>::iterador_const::anterior() {
	assert(valido());
	_pos = _pos->anterior;
}

template<class T>
void secu<T>::iterador_const::siguiente() {
	assert(valido());
	_pos = _pos->siguiente;
}

template<class T>
T& secu<T>::iterador_const::actual() const {
	assert(valido());
	return _pos->valor;
}

// iterador mutable

template<class T>
secu<T>::iterador::iterador() : secu<T>::iterador_const()
{
}

template<class T>
secu<T>::iterador::iterador(secu<T>& s) : secu<T>::iterador_const(s)
{
}

template<class T>
secu<T>::iterador::iterador(const typename secu<T>::iterador_const& i) :
	secu<T>::iterador_const(i)
{
}

// para los miembros derivados de iterador_const
// tengo que usar this->miembro en lugar de miembro a secas
// por una MUY oscura razon
// http://www.parashift.com/c++-faq-lite/templates.html#faq-35.19

// el Yuyo y la Calendula nos libren

#define valido	(this->valido)
#define _suby	(this->_suby)
#define _pos	(this->_pos)

template<class T>
void secu<T>::iterador::insertarAntes(const T& e)
{
	assert(valido());

	typename secu<T>::nodo *nuevo = new typename secu<T>::nodo;
	nuevo->valor = e;	// copia
	nuevo->anterior = _pos->anterior;
	nuevo->siguiente = _pos;

	++_suby->_longitud;
	if (_pos->anterior == NULL)
		_suby->_primero = nuevo;
	else
		_pos->anterior->siguiente = nuevo;
	_pos->anterior = nuevo;
}

template<class T>
void secu<T>::iterador::insertarDespues(const T& e)
{
	assert(valido());

	typename secu<T>::nodo *nuevo = new typename secu<T>::nodo;
	nuevo->valor = e;	// copia
	nuevo->anterior = _pos;
	nuevo->siguiente = _pos->siguiente;

	++_suby->_longitud;
	if (_pos->siguiente == NULL)
		_suby->_ultimo = nuevo;
	else
		_pos->siguiente->anterior = nuevo;
	_pos->siguiente = nuevo;
	return *this;
}

template<class T>
void secu<T>::iterador::borrarActual() {
	assert(valido());

	if (_pos->anterior != NULL)
		_pos->anterior->siguiente = _pos->siguiente;
	else
		_suby->_primero = _pos->siguiente;

	if (_pos->siguiente != NULL)
		_pos->siguiente->anterior = _pos->anterior;
	else
		_suby->_ultimo = _pos->anterior;

	--_suby->_longitud;

	typename secu<T>::nodo *sig = _pos->siguiente;
	delete _pos;
	_pos = sig;
}

// "corta" la secuencia
// desde donde esta el iterador en adelante
// (excluyendo el elemento actual)
// y devolviendo el sufijo en s2
template<class T>
void secu<T>::iterador::split(secu<T>& s2)
{
	assert(valido());

	s2.vaciar();

	// tengo que contar porque no conozco el indice del
	// elemento al que apunta el iterador =(
	typename secu<T>::nodo *p;
	unsigned int long2 = 0;
	for (p = _pos->siguiente; p != NULL; p = p->siguiente)
		++long2;

	if (long2 == 0) return;

	s2._primero = _pos->siguiente;
	s2._ultimo = _suby->_ultimo;
	s2._longitud = long2;
	_pos->siguiente->anterior = NULL;

	_suby->_ultimo = _pos;
	_pos->siguiente = NULL;
	_suby->_longitud -= long2;
}

#undef valido
#undef _suby
#undef _pos

// operator<<
template<class T>
std::ostream& operator<<(std::ostream& salida, const secu<T>& s)
{
	typename secu<T>::iterador_const it(s);

	salida << "[ ";
	while (it.valido()) {
		salida << it.actual() << " ";
		it.siguiente();
	}
	salida << "]";
	return salida;
}

#endif /*_SECU_H*/

