
// Diccionario con complejidades constantes

#ifndef _DICCCTE_H
#define _DICCCTE_H

#include <cassert>
#include <iostream>
#include <stdlib.h>

#include "conjlog.h"
#include "hash.h"

// asume un orden total C::operator<
// un C::operator==
// asume operadores de asignacion
//   C::operator=
//   V::operator=
template<class C, class V> class diccCte {
    
public:
	diccCte();
	~diccCte();
	diccCte(const diccCte<C, V>& d);
	diccCte<C, V>& operator=(const diccCte<C, V>& d);

	// operator== y operator!= requieren
	// ademas un V::operator==
	bool operator==( diccCte<C, V>& );
	bool operator!=( diccCte<C, V>& ) ;
    template<class C1, class V1> friend std::ostream&
         operator<<( std::ostream &out, const diccCte<C1,V1> &d );
    template<class C1, class V1> friend std::istream& 
         operator>>( std::istream &in, diccCte<C1,V1> &d );

	void definir(const C& clave, const V& valor);
	bool definido(const C& clave) const;
	V& obtener(const C& clave) const;
	diccCte<C, V>& borrar(const C& clave);
    conjlog<C> & claves();


private:

    struct _elem {
        C *clave;
        V *valor;
        bool libre;
        bool borrado;
    };

    struct _index {
        bool found;
        unsigned int index;
    };
    
    _elem *_bucket;
    unsigned int _bucketSize;
    unsigned int _nElems;

    unsigned int _hash( const C &key, unsigned int colision ) const;
    #define TAM_INICIAL_BUCKET   20
    #define REHASH_THRESHOLD    0.8
    
    _elem * _crearBucket(unsigned int n);
    void _byeByeLife();
    void _copiarBucket( const diccCte<C, V> & from, diccCte<C, V> & to );
    _index _definido(const C& clave) const;
    void _rehash();
};


template <class C, class V>
typename diccCte<C, V>::_elem * diccCte<C, V>::_crearBucket(unsigned int n) {
    unsigned int i;

    typename diccCte<C, V>::_elem e;
    e.libre = true;
    e.borrado = false; // No tiene validez sem�ntica, pero evita un warning :-D
    e.valor = NULL;
    e.clave = NULL;

    typename diccCte<C, V>::_elem *thisBucket;
    thisBucket = new typename diccCte<C, V>::_elem[n];

    for ( i=0; i < n; i++) {
        thisBucket[i] = e;
    };
    
    return thisBucket;
};

template <class C, class V>
void diccCte<C, V>::_byeByeLife()
{
	unsigned int i;

	for (i = 0; i < _bucketSize; i++ ) {
		if ( _bucket[i].clave != NULL )
			delete _bucket[i].clave;
		if ( _bucket[i].valor != NULL )
			delete _bucket[i].valor;
	};

    delete [] _bucket;
};

template <class C, class V>
void diccCte<C, V>::_copiarBucket( const diccCte<C, V> & from, diccCte<C, V> & to ) {
	unsigned int i;

	for (i = 0; i < from._bucketSize; i++ ) {
		to._bucket[i] = from._bucket[i];
		if ( from._bucket[i].clave != NULL ) {
			to._bucket[i].clave = new C;
			*(to._bucket[i].clave) = *(from._bucket[i].clave);
			to._bucket[i].valor = new V;
			*(to._bucket[i].valor) = *(from._bucket[i].valor);
		};
	};
};


template <class C, class V>
void diccCte<C, V>::_rehash() {
	_elem  *_newBucket = _crearBucket( _bucketSize * 2 );
	_bucketSize *= 2;
	
	for (unsigned int i = 0; i < _bucketSize/2; i++ ) {
		unsigned int colision = 1;
		unsigned int dest = _hash( *(_bucket[i].clave), colision );
		while (  ! _bucket[dest].libre && ! _bucket[dest].borrado ) {
			colision++;
			 dest = _hash( *(_bucket[i].clave), colision );
		};
	
		_newBucket[dest].valor = _bucket[i].valor; 
		_newBucket[dest].clave = _bucket[i].clave;
		_newBucket[dest].borrado = false;
		_newBucket[dest].libre = false;
	};

	delete [] _bucket;
	_bucket = _newBucket;
};

template <class C, class V>
diccCte<C, V>::diccCte()
{
    _bucket = _crearBucket(TAM_INICIAL_BUCKET);
    _bucketSize = TAM_INICIAL_BUCKET;
    _nElems = 0;
}

template <class C, class V>
diccCte<C, V>::~diccCte()
{
	_byeByeLife();
}

template <class C, class V>
diccCte<C, V>::diccCte(const diccCte<C, V>& d)
{
	_bucket = _crearBucket(d._bucketSize);
	_bucketSize = d._bucketSize;
	_copiarBucket( d, *this );
	_nElems = d._nElems;
}

template <class C, class V>
unsigned int diccCte<C, V>::_hash( const C &key, unsigned int colision ) const {
    return ( hash( colision, key ) % _bucketSize );
};

template <class C, class V>
diccCte<C, V>& diccCte<C, V>::operator=( const diccCte<C, V>& d )
{
	if (this != &d) {
		_byeByeLife();
		d._bucket = _crearBucket(_bucketSize);
		d._bucketSize = _bucketSize;
		_copiarBucket(  d, *this );
		_nElems = d._nElems;
	}
	return *this;
}


template <class C, class V>
bool diccCte<C, V>::operator==( diccCte<C, V>& d )
{
	conjlog<C>& k = claves();
	conjlog<C>& dKeys = d.claves();
	
	if ( ! ( k == dKeys ) ) {
		delete &k;
		delete &dKeys;
		return false;
	};
	
	delete &dKeys;
		
	typename conjlog<C>::iterador_const it = typename conjlog<C>::iterador_const( k );
	unsigned int i1, i2;

	while ( it.valido() ) {
		i1 = _definido(it.actual()).index;
		i2 = d._definido(it.actual()).index;
		if ( ! ( *(_bucket[ i1 ].valor) ==  *(d._bucket[ i2 ].valor) )) {
			delete &k;
			return false;
		};
		it.siguiente();
	};
	
	delete &k;
	return true;
}

template <class C, class V>
bool diccCte<C, V>::operator!=( diccCte<C, V>& d) 
{
	return !(*this == d);
}

template <class C, class V>
void diccCte<C, V>::definir(const C& clave, const V& valor)
{

	_index este = _definido(clave);
	
	if ( este.found ) {
		*(_bucket[este.index].valor) = valor;
		return;
	};
	
	if ( ( _nElems /_bucketSize ) > REHASH_THRESHOLD )
		 _rehash();

	unsigned int colision = 1;
	unsigned int i = _hash(clave,colision);
	while (  ! _bucket[i].libre && ! _bucket[i].borrado ) {
		colision++;
		i = _hash(clave,colision);
	};

	if (_bucket[i].valor != NULL)
		delete _bucket[i].valor;
	if (_bucket[i].clave != NULL)
		delete _bucket[i].clave;
	_bucket[i].valor = new V;
	*(_bucket[i].valor) = valor;
	_bucket[i].clave = new C;
	*(_bucket[i].clave) = clave;
	_bucket[i].borrado = false;
	_bucket[i].libre = false;
	_nElems++;
}

template <class C, class V>
typename diccCte<C, V>::_index diccCte<C, V>::_definido(const C& clave) const
{
	unsigned int i;
	typename diccCte<C, V>::_index ret;
	
	for( i = 0; i < _bucketSize; i++ ) {
		if ( ! _bucket[i].libre && ! _bucket[i].borrado &&
			 *(_bucket[i].clave) == clave ) {

			ret.index = i;
			ret.found = true;
			return ret;
		};
	};
	
	ret.index = 0;  // evita warnings !!!
	ret.found = false;
			
	return ret;
}

template <class C, class V>
bool diccCte<C, V>::definido(const C& clave) const
{
	return _definido(clave).found;
}

template <class C, class V>
V& diccCte<C, V>::obtener(const C& clave) const
{
	unsigned int colision = 1;
	unsigned int i = _hash(clave,colision);
	bool found = false;

	while ( ! found ) {

		if (_bucket[i].borrado) {
			colision++;
			i = _hash(clave,colision);
		} else if ( *(_bucket[i].clave) == clave ) {
			found = true;
		} else { // esta libre, no hay mas que buscar, no se cumple la pre ...
			assert(false);
		}
	};
	
	return *(_bucket[i].valor);
}

template <class C, class V>
conjlog<C> & diccCte<C, V>::claves() {
    unsigned int i;
    conjlog<C> *claves = new conjlog<C>();

    for( i = 0; i < _bucketSize; i++ ) {
        if ( ! _bucket[i].libre &&  ! _bucket[i].borrado ) {
            claves->agregar(  *(_bucket[i].clave) );
        }
    };

    return *claves;
};

#endif /*_DICCCTE_H*/

