

#include <math.h>
#include "hash.h"

using namespace std;

unsigned int hash( unsigned int colision, int v ) {
	    unsigned int myHash =  abs(v) + colision ;
		return myHash;
};

unsigned int hash( string v, unsigned int inicio, unsigned int fin ) {
	unsigned int hash, i;

	for( i=inicio, hash=0; i < fin; i++ ) {
    	hash += (unsigned int) ( v[i] * pow( (double) 2, (double) i ) );
    };
    
	return hash;	
};

unsigned int hash( unsigned int colision,  string valor) {
	unsigned int myHash = ( hash( valor, 0, valor.length() / 2 ) + 
    	     			    colision * ( hash( valor, valor.length() / 2, valor.length() ) ) 
    	   				   );
	return myHash;
};

unsigned int hash(unsigned int colision, const Agujero a){	
	unsigned int myHash; 
	if ( a.unaPunta < a.otraPunta )
		myHash = hash(colision,a.unaPunta + a.otraPunta);
	else
		myHash = hash(colision,a.otraPunta + a.unaPunta);

	return myHash;

}

