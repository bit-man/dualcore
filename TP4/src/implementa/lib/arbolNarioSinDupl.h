#ifndef ARBOLNARIOSINDUPL_H_
#define ARBOLNARIOSINDUPL_H_

#include "conjlog.h"

// asume un orden total C::operator<
// un C::operator==
// asume operadores de asignacion
//   C::operator=
//   V::operator=

template<class T> class arbolNarioSinDupl {
	public:
		arbolNarioSinDupl();
        // equivalente a la operacion nin()
		arbolNarioSinDupl( const T & padre, const conjlog<arbolNarioSinDupl<T> > & hijos );  
        ~arbolNarioSinDupl();

		bool vacio();
		T & raiz() const;
		conjlog<arbolNarioSinDupl<T> > & hijos() const;
		bool esta( const T & esteValor ) const;
		unsigned int altura() const;

        arbolNarioSinDupl<T>& operator=(const arbolNarioSinDupl<T>& a);
        void agregarHijo( const T & padre, const arbolNarioSinDupl<T>& a );
        conjlog<T>& valoresHijos( const T & padre );
		bool operator==(const arbolNarioSinDupl<T>& a) const;
		bool operator<(const arbolNarioSinDupl<T>& a) const;

		template<class T1> friend std::ostream&
			operator<<(std::ostream& salida, const arbolNarioSinDupl<T1>& a);

   
	private:
	
		typedef struct _celda {
			T *valor;
			conjlog<arbolNarioSinDupl<T> > *hijos;
		} celda;
		
		struct _estaDonde {
			bool esta;
			celda *donde;
		};
		
		celda *_head;

		_estaDonde _acaEsta( const T & esteValor ) const;
        void _crearCelda(const T& padre, const conjlog<arbolNarioSinDupl<T> > & hijos );
        void _byeByeLife();
};

template<class T>
void arbolNarioSinDupl<T>::_crearCelda(const T& padre, const conjlog<arbolNarioSinDupl<T> > & hijos ) {

    _head = new typename arbolNarioSinDupl<T>::celda;

    _head->valor = new T;
    *(_head->valor) = padre;

    _head->hijos = new conjlog<arbolNarioSinDupl<T> >;
    *(_head->hijos) = hijos;

};

template<class T>
void arbolNarioSinDupl<T>::_byeByeLife() {
	if (_head != NULL ) {
		if ( _head->valor != NULL )
			delete _head->valor;
		if ( _head->hijos != NULL )
			delete _head->hijos;
		delete _head;
	};
};

template<class T>
typename arbolNarioSinDupl<T>::_estaDonde arbolNarioSinDupl<T>::_acaEsta( const T & esteValor ) const {
	_estaDonde ret;
	
	if ( *(_head->valor) == esteValor ) {
		ret.esta = true;
		ret.donde = _head;
		return ret;
	};

    typename conjlog<arbolNarioSinDupl<T> >::iterador_const it( *(_head->hijos) );

	it.comenzar();
	while ( it.valido() ) {
		_estaDonde aux = it.actual()._acaEsta( esteValor );
		if  ( aux.esta ) {
			ret.esta = true;
			ret.donde = aux.donde;
			return ret;
		};
		it.siguiente();
	};

	ret.esta = false;
	ret.donde = NULL;
	return ret;
};

template<class T>
arbolNarioSinDupl<T>& arbolNarioSinDupl<T>::operator=(const arbolNarioSinDupl<T>& a) {
    if ( this != &a ) {
        _byeByeLife();

        if ( (&a != NULL ) && (a._head != NULL) ) {

	      	_crearCelda( a.raiz(), a.hijos() );

        } else { 
    		_head = NULL;
        };
    };
    return *this;
};

template<class T> 
arbolNarioSinDupl<T>::arbolNarioSinDupl() {
	_head = NULL;
};


template<class T> 
arbolNarioSinDupl<T>::arbolNarioSinDupl( const T & padre, const conjlog<arbolNarioSinDupl<T> > & hijos ) {
	_crearCelda( padre, hijos );
};


template<class T> 
arbolNarioSinDupl<T>::~arbolNarioSinDupl() {
    _byeByeLife();
};


template<class T> 
bool arbolNarioSinDupl<T>::vacio() {
	return ( _head == NULL );
};


template<class T> 
T & arbolNarioSinDupl<T>::raiz() const {
	return *(_head->valor);
};


template<class T> 
conjlog<arbolNarioSinDupl<T> > & arbolNarioSinDupl<T>::hijos() const {
	return *(_head->hijos);
};


template<class T> 
bool arbolNarioSinDupl<T>::esta( const T & esteValor ) const {
	return _acaEsta(esteValor).esta;
};


template<class T>  
unsigned int arbolNarioSinDupl<T>::altura() const {
    unsigned int altura = 0;
    unsigned int estaAltura;

    typename conjlog<arbolNarioSinDupl<T> >::iterador_const it( *(_head->hijos) );

	it.comenzar();
	while ( it.valido() ) {
        estaAltura = it.actual().altura();
		if  ( estaAltura > altura ) 
			altura = estaAltura;
		it.siguiente();
	};
    
	return altura + 1;
};

template<class T>
void arbolNarioSinDupl<T>::agregarHijo( const T & padre, const arbolNarioSinDupl<T>& a ) {
	_estaDonde elPadre = _acaEsta( padre );
	assert( elPadre.esta );
	elPadre.donde->hijos->agregar( a );
};

template<class T>
conjlog<T>& arbolNarioSinDupl<T>::valoresHijos( const T & padre ) {
	_estaDonde elPadre = _acaEsta( padre );
	assert( elPadre.esta );

	conjlog<T> *res = new conjlog<T>();
	typename conjlog<arbolNarioSinDupl<T> >::iterador_const it ( *(elPadre.donde->hijos) ); 
	
	it.comenzar();
	while (it.valido() ) {
		res->agregar( it.actual().raiz() );
		it.siguiente();
	}
	return *res;
};

template<class T> 
bool arbolNarioSinDupl<T>::operator==(const arbolNarioSinDupl<T>& a) const {

	return ( !(*this < a) && !( a < *this) );
};


template<class T> 
bool arbolNarioSinDupl<T>::operator<(const arbolNarioSinDupl<T>& a) const {


	if ( (_head == NULL) && (a._head == NULL) ) {  // ambos son NULL simult�neamente

		return false;
	} else if ( (a._head != NULL) && (_head == NULL) ) {

		return true;
	} else if ( (a._head == NULL) && (_head != NULL) ) {

		return false;
	} else {	// ninguno es NULL
		return ( *(_head->valor) < *(a._head->valor) );
	};
};


template<class T1> 
std::ostream&  operator<<(std::ostream& salida, const arbolNarioSinDupl<T1>& a) {

	salida << "< ";

    if ( a._head != NULL ) {
        salida << "#" << *(a._head->valor) << " ";
	
		typename conjlog<arbolNarioSinDupl<T1> >::iterador_const it ( *(a._head->hijos) ); 
	
		it.comenzar();
		while (it.valido() ) {
			salida << it.actual().raiz();
			it.siguiente();
		};
    };
    
    salida << ">";
	return salida;
}


#endif /*ARBOLNARIOSINDUPL_H_*/
