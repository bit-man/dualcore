// Conjunto con complejidades logar'itmicas

#ifndef _CONJLOG_H
#define _CONJLOG_H

#include <cassert>
#include <iostream>

#include "secu.h"
#include "dicclog.h"

// asume un orden total T::operator< y un T::operator==
// asume un operador de asignacion T::operator=
template<class T> class conjlog {
public:
	conjlog();
	conjlog(const conjlog<T>& c);
	conjlog<T>& operator=(const conjlog<T>& c);

	bool operator==(const conjlog<T>& c) const;
	bool operator!=(const conjlog<T>& c) const;

	conjlog<T>& agregar(const T& x);
	bool pertenece(const T& x) const;
	conjlog<T>& sacar(const T& x);

	const T& dameUno() const;
	conjlog<T>& sinUno();

	unsigned int cardinal() const;
	bool esVacio() const;

	// grr, no podemos poner union porque es palabra reservada
	conjlog<T>& unir(const conjlog<T>& c);
	conjlog<T>& interseccion(const conjlog<T>& c);
	conjlog<T>& diferencia(const conjlog<T>& c);
	conjlog<T>& diferencia_simetrica(const conjlog<T>& c);

	// *this (= c
	bool incluidoEn(const conjlog<T>& c) const;
	// c (= *this
	bool incluye(const conjlog<T>& c) const;

	// requiere operator<< para T
	template<class T1> friend std::ostream&
		operator<<(std::ostream& salida, const conjlog<T1>& c);
	// requiere operator>> para T
	template<class T1> friend std::istream&
		operator>>(std::istream& entrada, conjlog<T1>& c);
private:
	// implementado como un diccionario de elemento en bool
	dicclog<T, bool> _conj;

	secu<const T *> *_armar_secu_definicion(const conjlog<T>& c, bool esta)
		const;
	void _borrar_elementos_en_secu(secu<const T *> *s);
public:
	// iterador constante
	class iterador_const {
	public:
		iterador_const();
		iterador_const(const conjlog<T>& c);
		iterador_const(const typename conjlog<T>::iterador_const& i);
		typename conjlog<T>::iterador_const&
			operator=(const typename conjlog<T>::iterador_const& i);
		bool operator==(const typename conjlog<T>::iterador_const& i);
		bool operator!=(const typename conjlog<T>::iterador_const& i);

		void comenzar();
		void siguiente();
		bool valido() const;
		const T& actual() const;
		
		
	protected:
		typename dicclog<T, bool>::iterador _it;
	};

	// iterador mutable
	class iterador : public conjlog<T>::iterador_const {
	public:
		iterador();
		iterador(conjlog<T>& c);
		iterador(const typename conjlog<T>::iterador_const& i);

		// borra el actual
		// *invalida el iterador*
		void borrarActual();
	};
};

template <class T>
conjlog<T>::conjlog()
{
}

template <class T>
conjlog<T>::conjlog(const conjlog<T>& c)
{
	_conj = c._conj;
}

template <class T>
conjlog<T>& conjlog<T>::operator=(const conjlog<T>& c)
{
	_conj = c._conj;
	return *this;
}

template <class T>
bool conjlog<T>::operator==(const conjlog<T>& c) const
{
	// asumimos T::operator==
	return _conj == c._conj;
}

template <class T>
bool conjlog<T>::operator!=(const conjlog<T>& c) const
{
	return !(*this == c);
}

template <class T>
conjlog<T>& conjlog<T>::agregar(const T& x)
{
	if (!_conj.definido(x))
		_conj.definir(x, true);
	return *this;
}

template <class T>
bool conjlog<T>::pertenece(const T& x) const
{
	return _conj.definido(x);
}

template <class T>
conjlog<T>& conjlog<T>::sacar(const T& x)
{
	_conj.borrar(x);
	return *this;
}

template <class T>
unsigned int conjlog<T>::cardinal() const
{
	return _conj.tamano();
}

template <class T>
bool conjlog<T>::esVacio() const
{
	return _conj.tamano() == 0;
}

// O(log(n) * m)
// donde n y m son los cardinales de cada conjunto
template <class T>
conjlog<T>& conjlog<T>::unir(const conjlog<T>& c)
{
	typename dicclog<T, bool>::iterador it(c._conj);
	while (it.valido()) {
		const T &elem = it.claveActual();
		if (!_conj.definido(elem))
			_conj.definir(elem, true);
		it.siguiente();
	}
	return *this;
}

// O(n * (log(n) + log(m)))
// donde n y m son los cardinales de cada conjunto
template <class T>
conjlog<T>& conjlog<T>::interseccion(const conjlog<T>& c)
{
	secu<const T *> *para_borrar = _armar_secu_definicion(c, false);
	_borrar_elementos_en_secu(para_borrar);
	delete para_borrar;
	return *this;
}

// O(log(n) * m)
// donde n y m son los cardinales de cada conjunto
template <class T>
conjlog<T>& conjlog<T>::diferencia(const conjlog<T>& c)
{
	typename dicclog<T, bool>::iterador it(c._conj);

	while (it.valido()) {
		const T &elem = it.claveActual();
		_conj.borrar(elem);
		it.siguiente();
	}
	return *this;
}

// O((n + m) (log(n) + log(m)))
// donde n y m son los cardinales de cada conjunto
template <class T>
conjlog<T>& conjlog<T>::diferencia_simetrica(const conjlog<T>& c)
{
	// XXX: ver como esto se podria hacer mejor.
	// lo que hacemos ahora es copiar todos los
	// elementos de c al hacer la union,
	// y despues borrar los que estaban en la
	// interseccion.
	// => esta copiando innecesariamente los
	//    elementos que ya estaban en la interseccion
	secu<const T *> *para_borrar = _armar_secu_definicion(c, true);
	unir(c);
	_borrar_elementos_en_secu(para_borrar);
	delete para_borrar;
	return *this;
}

// O(n log(m))
// donde n y m son los cardinales de cada conjunto
//
// devuelve una secuencia de (punteros a) elementos que
// - estan en *this
// - estan en c <=> esta
template <class T>
secu<const T *> *
conjlog<T>::_armar_secu_definicion(const conjlog<T>& c, bool esta) const {
	secu<const T *> *s = new secu<const T *>;

	typename dicclog<T, bool>::iterador_const it(_conj);
	while (it.valido()) {
		const T &elem = it.claveActual();
		if (c._conj.definido(elem) == esta)
			s->insertar(&elem);
		it.siguiente();
	}
	return s;
}

// O(n log(n))
// donde n y m es el cardinal del conjunto
template <class T>
void conjlog<T>::_borrar_elementos_en_secu(secu<const T *> *s) {
	typename secu<const T *>::iterador is(*s);
	while (is.valido()) {
		_conj.borrar(*is.actual());
		is.siguiente();
	}
}

template <class T>
const T& conjlog<T>::dameUno() const
{
	assert(cardinal() > 0);
	typename dicclog<T, bool>::iterador_inorder it(_conj);
	return it.claveActual();
}

template <class T>
conjlog<T>& conjlog<T>::sinUno()
{
	assert(cardinal() > 0);
	typename dicclog<T, bool>::iterador_inorder it(_conj);
	it.borrarActual();
	return *this;
}

// O(n * log(m))
// donde n y m son los cardinales de cada conjunto
template <class T>
bool conjlog<T>::incluidoEn(const conjlog<T>& c) const
{
	typename dicclog<T, bool>::iterador_const it(_conj);
	while (it.valido()) {
		if (!c._conj.definido(it.claveActual()))
			return false;
		it.siguiente();
	}
	return true;
}

template <class T>
bool conjlog<T>::incluye(const conjlog<T>& c) const
{
	return c.incluidoEn(*this);
}

// iterador constante
template <class T>
conjlog<T>::iterador_const::iterador_const()
{
}

template <class T>
conjlog<T>::iterador_const::iterador_const(const conjlog<T>& c) 
{
	_it = typename dicclog<T, bool>::iterador(c._conj);
}

template <class T>
conjlog<T>::iterador_const::iterador_const(const typename conjlog<T>::iterador_const& i)
{
	_it = i._it;
}

template <class T>
typename conjlog<T>::iterador_const&
conjlog<T>::iterador_const::operator=(const typename conjlog<T>::iterador_const& i)
{
	_it = i._it;
	return *this;
}

template <class T>
bool conjlog<T>::iterador_const::operator==(const typename conjlog<T>::iterador_const& i)
{
	return _it == i._it;
}

template <class T>
bool conjlog<T>::iterador_const::operator!=(const typename conjlog<T>::iterador_const& i)
{
	return _it != i._it;
}

template <class T>
void conjlog<T>::iterador_const::comenzar()
{
	_it.comenzar();
}

template <class T>
void conjlog<T>::iterador_const::siguiente()
{
	_it.siguiente();
}

template <class T>
bool conjlog<T>::iterador_const::valido() const
{
	return _it.valido();
}

template <class T>
const T& conjlog<T>::iterador_const::actual() const
{
	return _it.claveActual();
}

// iterador mutable
template <class T>
conjlog<T>::iterador::iterador() : conjlog<T>::iterador_const::iterador_const()
{
}

template <class T>
conjlog<T>::iterador::iterador(conjlog<T>& c) :
	conjlog<T>::iterador_const::iterador_const(c)
{
}

template <class T>
conjlog<T>::iterador::iterador(const typename conjlog<T>::iterador_const& i) :
	conjlog<T>::iterador_const::iterador_const(i)
{
}

template <class T>
void conjlog<T>::iterador::borrarActual()
{
	this->_it.borrarActual();
}

// operator<<
template<class T>
std::ostream& operator<<(std::ostream& salida, const conjlog<T>& c) {
	typename dicclog<T, bool>::iterador it(c._conj);

	salida << "{ ";
	while (it.valido()) {
		salida << it.claveActual() << " ";
		it.siguiente();
	}
	salida << "}";
	return salida;
}

// operator>>
template<class T>
std::istream& operator>>(std::istream& entrada, conjlog<T>& c) {
	char siguiente;
	T dato;

	entrada >> siguiente;
	assert( siguiente == '{' );

	while(true) {
		entrada >> siguiente;
		if(siguiente == '}')
			break;
		entrada.putback(siguiente);
		entrada >> dato;
		c.agregar(dato);
	}

	return entrada;
}

#endif /*_CONJLOG_H*/

