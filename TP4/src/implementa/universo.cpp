
#include "universo.h"


/******
 *  el destructor no estaba en el public original
 ******/

Universo::~Universo() {
	if (_galaxias != NULL )
	    delete _galaxias;
    if (_tostadoras != NULL )
        delete _tostadoras;
    if (_agujeros != NULL )
        delete _agujeros;
	if(campeonaPointer!=NULL)
		delete campeonaPointer;
};


Universo::Universo(const ConjGalaxia& cg):campeonaPointer(NULL) {

	_hayCampeona = false;

	_campeona.nombreT = "";
	_campeona.claseT = Tostadora::classic;
    _campeona.viajesT = 0; 
	

	_agujeros = new diccCte<Agujero,Nat>;

	_tostadoras = new dicclog<string,signTost>;

	_galaxias = new conjlog<Galaxia>;
	*_galaxias = cg;

};

void Universo::agTostadora(const Tostadora& t, const Planeta& p) {

	Universo::signTost *st = new Universo::signTost;
	st->pos = p;
	st->cantViajes = 0;
	st->clase = t.clase();
	
	if ( _tostadoras->tamano() == 0 ) {

		_hayCampeona = true;
		_campeona.nombreT = t.nombre();
		_campeona.viajesT = st->cantViajes;
		_campeona.claseT = st->clase;
		campeonaPointer = new Tostadora(t.nombre(),st->clase);
	} else if ( _campeona.viajesT == st->cantViajes ) {
		delete campeonaPointer;
		campeonaPointer = NULL;
		_hayCampeona = false;
	};
		
	_tostadoras->definir(t.nombre(), *st);


	delete st;
};

Agujero & Universo::_normalizarAgujero( const Agujero & a ) {
	Agujero *ret = new Agujero;
	if ( a.unaPunta < a.otraPunta ) {
		ret->unaPunta = a.unaPunta;
		ret->otraPunta = a.otraPunta;
	} else {
		ret->otraPunta = a.unaPunta;
		ret->unaPunta = a.otraPunta;
	};
	
	return *ret;
};

void Universo::abrirAgujero(const Agujero& a) {
	
	Agujero & ax = _normalizarAgujero(a);
 	_agujeros->definir( ax ,5 );
 	delete &ax;
};
	

void Universo::ordenarViaje(const Tostadora& t, const Planeta& dest) {

	signTost st = _tostadoras->obtener(t.nombre());
	Planeta origen = st.pos;
	Galaxia & gDest = _dameGalaxia (dest, *_galaxias);
	Galaxia & gOrigen = _dameGalaxia (origen, *_galaxias);
	conjlog<Planeta>& cpDest = _planetasG(gDest);
	conjlog<Planeta>& cpOrigen = _planetasG(gOrigen);

	
	if (origen == dest) {
		delete &cpDest;
		delete &cpOrigen;
		delete &gOrigen;
		delete &gDest;
		return;
	};

	if ( cpOrigen.pertenece(dest) ) {	

		st.pos = dest;	
		st.cantViajes++;
	} else {

		Universo::agujeroEncontrado planetasAgujero = _dameUnAgujero(cpDest, cpOrigen);                

		if( planetasAgujero.encontreAgujero ) {
			st.pos = dest;		
			st.cantViajes++;
			
			Nat v = _agujeros->obtener(planetasAgujero.adg) - 1;
						
			_agujeros->definir( planetasAgujero.adg, v );	


		} else {

			if (st.clase == Tostadora::enterprise) {

				Agujero *agujeroUsado = new Agujero;
				agujeroUsado->unaPunta = gDest.centro();
				agujeroUsado->otraPunta = gOrigen.centro();
				
				abrirAgujero(*agujeroUsado);
				// consume el primer viaje
				Agujero & agujeroUsado2 = _normalizarAgujero( *agujeroUsado );
	
				st.pos = dest;
				st.cantViajes++;

				delete agujeroUsado;
				delete &agujeroUsado2;	
			}	
		};	
	}

	if (! (origen == st.pos) )
		_tostadoras->definir(t.nombre(), st);

	// actualización de campeona



	if (_hayCampeona) {
	
	 	if (! (t.nombre() == _campeona.nombreT) ) { 
			if (st.cantViajes == _campeona.viajesT) {
	
				_hayCampeona = false;
				delete campeonaPointer;
				campeonaPointer = NULL;
			};
	 	} else {
	
			_campeona.viajesT = st.cantViajes;	
	 	}
	} else {
	
		if (st.cantViajes > _campeona.viajesT) {
	
				_campeona.nombreT = t.nombre();
				_campeona.viajesT = st.cantViajes;
				_campeona.claseT = t.clase();	
				_hayCampeona = true;
				campeonaPointer = new Tostadora(t.nombre(),t.clase());
		}
	};
	
	delete &cpDest;
	delete &cpOrigen;
	delete &gOrigen;
	delete &gDest;
};
				

conjlog<Planeta>& Universo::_planetasG( const Galaxia & g ) {
	conjlog<Planeta> *cp = new conjlog<Planeta>;
	conjlog<Planeta> *bolsaEspera = new conjlog<Planeta>;
	
	Galaxia *gAux = new Galaxia(g);
	bolsaEspera->agregar( gAux->centro() );
	while( ! bolsaEspera->esVacio() ) { 
		Planeta pp = bolsaEspera->dameUno();
		bolsaEspera->sinUno();
		cp->agregar( pp );
		conjlog<Planeta>::iterador_const it( gAux->sats(pp) );
		it.comenzar();
		while ( it.valido() ) {			
			cp->agregar( it.actual() );
			
			conjlog<Planeta>::iterador_const it2( gAux->sats( it.actual() ) );
			it2.comenzar();
			while ( it2.valido() ) {
				bolsaEspera->agregar( it2.actual() );
				it2.siguiente();	
			};
			
			it.siguiente();
		};
	};
	
	delete bolsaEspera;
	delete gAux;

	return *cp;
};

Galaxia & Universo::_dameGalaxia( const Planeta &p, conjlog<Galaxia> & cg ) {
    
    conjlog<Galaxia>::iterador_const it( cg );
    it.comenzar();
    conjlog<Planeta> *cp = new conjlog<Planeta>;
    *cp = _planetasG( it.actual() );
    while ( ! cp->pertenece(p) ) {
            it.siguiente();
            *cp = _planetasG( it.actual() );
    };
    delete cp;
	
	Galaxia *gAux = new Galaxia( it.actual() ); 
	return *gAux;
};


Universo::agujeroEncontrado Universo::_dameUnAgujero(conjlog<Planeta>& cp, conjlog<Planeta>& cq){

	

	conjlog<Agujero> *ca = new conjlog<Agujero>;
		
	Universo::agujeroEncontrado *ae = new Universo::agujeroEncontrado;
	ae->encontreAgujero = false;
	
	conjlog<Planeta>::iterador_const it( cp );
			it.comenzar();
	while ( it.valido() ){
		conjlog<Planeta>::iterador_const it2( cq );
		it2.comenzar();
		
		while( it2.valido() ){
		
			Agujero *adg = new Agujero;
			adg->unaPunta = it.actual();
			adg->otraPunta = it2.actual();
			if ( hayAgujero(*adg) ) {
				ae->adg = _normalizarAgujero(*adg);
				if ( _agujeros->obtener(ae->adg) != 0 ){
					ca->agregar(ae->adg);
				};
			};	
			it2.siguiente();
			delete adg;
		}
		it.siguiente();
	}
		
	ae->encontreAgujero = !(ca->esVacio());
	if (ae->encontreAgujero)
		ae->adg = ca->dameUno();
	
	delete ca;
	
	return *ae;		
}



bool Universo::esGalaxia(const Galaxia& g) {

    return _galaxias->pertenece(g);
};


bool Universo::esTostadora(const Tostadora& t) {
	if ( ! _tostadoras->definido( t.nombre() ) )
		return false;
	
	signTost *tAux = new signTost; 
	*tAux = _tostadoras->obtener( t.nombre() );
	bool ret = ( tAux->clase == t.clase() );
	delete tAux;
    return  ret;
};


const Planeta& Universo::pos(const Tostadora& t) {
    return _tostadoras->obtener( t.nombre() ).pos;
};


int Universo::viajes(const Tostadora& t) {

    return _tostadoras->obtener(t.nombre()).cantViajes;
};


bool Universo::hayAgujero(const Agujero& a) {

	Agujero & norm = _normalizarAgujero(a);
	bool def = (_agujeros->definido(norm) && ( _agujeros->obtener(norm) != 0 ) );
	delete &norm;
    return  def;
};


int Universo::energia(const Agujero& a) {
	Agujero & norm = _normalizarAgujero(a);
	int ret = _agujeros->obtener(norm);
	delete &norm;
    return  ret;
};


bool Universo::hayCampeona() {
    return _hayCampeona;
};


const Tostadora Universo::campeona() {
	assert (_hayCampeona && (campeonaPointer != NULL));
    return *campeonaPointer;
};
