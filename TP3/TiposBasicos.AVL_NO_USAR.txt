==============================================================================================================================================================

Reference :
Donald Knuth. The Art of Computer Programming, Volume 3: Sorting and Searching, 
Third Edition. Addison-Wesley, 1997. ISBN 0-201-89685-0.
Pages 458?475 of section 6.2.3: Balanced Trees. Note that Knuth calls AVL trees simply "balanced trees".

avl( \alfa ) se representa con estr
donde estr es puntero( nodo )
donde nodo es tupla< valor: \alfa  x  padre: puntero(dato)  x
                     left:  puntero(dato)  x  right:  puntero(dato) >

Rep: estr --> bool
Rep(e) = esABB(e) \and alturaDeCadaSubarbolDifiereEnMenosDe2(e) 
         \and cadaHijoTiene�nicoPadre(e) \and punterosAPadreCorrectos(e)

esABB: estr --> bool
esABB(e) = if e = null then
                true
           else
                esMayorQueSuHijoIzq(e) \and esMenorQueSuHijoDer(e) \and
                esABB( e->left ) \and esABB( e->right )
           fi

esMayorQueSuHijoIzq: estr --> bool                      \not vac�o(e)
esMayorQueSuHijoIzq(e) = hayArbolIzq(e) =>L e->valor > e->left->valor

esMenorQueSuHijoDer: estr --> bool                      \not vac�o(e)
esMenorQueSuHijoDer(e) = hayArbolDer(e) =>L e->valor < e->right->valor

hayArbolIzq: estr --> bool                              \not vac�o(e)
hayArbolIzq(e) =  \not vac�o( e->left )

hayArbolDer: estr --> bool                              \not vac�o(e)
hayArbolDer(e) =  \not vac�o( e->right )

vac�o: estr --> bool
vac�o(e) = (e = null)

alturaDeCadaSubarbolDifiereEnMenosDe2: estr --> bool
alturaDeCadaSubarbolDifiereEnMenosDe2(e) =
    if e = null then
        true 
    else
        absDiff( altura( e->left ), altura( e->right ) ) < 2  \and
        alturaDeCadaSubarbolDifiereEnMenosDe2( e->left ) \and
        alturaDeCadaSubarbolDifiereEnMenosDe2( e->right )
    fi

absDiff: nat x nat --> nat
absDiff(a,b) = if a > b then   
                    a - b
               else
                    b - a
               fi

cadaHijoTiene�nicoPadre: estr --> bool
cadaHijoTiene�nicoPadre(e) = 
        ( \paratodo p: puntero(nodo) ) (#punteros( p, e ) =< 1)

#punteros: puntero(nodo) x estr --> nat
#punteros(p,e) = if e = null then 
                     0
                 else 
                     [if p = e then 1 else 0] + #punteros( p, e->left ) +
                     #punteros( p, e->right )
                 fi

punterosAPadreCorrectos: estr --> bool
punterosAPadreCorrectos(e) = if e = null then
                                true
                             else
                                punteroAHijoEsCorrecto( e, e->left ) \and 
                                punteroAHijoEsCorrecto( e, e->right ) \and
                                punterosAPadreCorrectos( e->left ) \and
                                punterosAPadreCorrectos( e->right )
                             fi

punteroAHijoEsCorrecto: estr x estr --> bool
punteroAHijoEsCorrecto(padre,hijo) = (hijo = null) \or_l (padre = hijo->padre)

agregarOrdenado: \alfa  x  secu(\alfa) --> secu(\alfa)
agregarOrdenado( elem, s ) = if vacia?(s) then
                                 elem . <> 
                             else
                                 if elem < prim(s) then
                                     elem . s
                                 else
                                     prim(s) . agregarOrdenado( elem, fin(s) )
                                 fi
                             fi
Abs: estr e --> avl(\alfa)              Rep(e)
( \paratodo e: estr) Abs(e) = a: avl(\alfa) /
                              ( nil?(a) = (e = null) ) \and_l
                              \not nil?(a) =>L ( ( raiz(a) = e->valor ) \and
                                                 ( izq(a) = e->left ) \and
                                                 ( der(a) = e->right ) 
                              )


Interfaz
    se explica con especificacion arbol binario(\alfa)
    usa interfaces BOOL, NAT, \ALFA
    genero avl(\alfa)
    
    operaciones:

    NilAVL?( in a: avl(\alfa) ) --> res: bool                               O(1)
    { true }
    { res = nil?(a) }

    RaizAVL( in a: avl(\alfa) ) --> res: \alfa                              O(1)
    { \not nil?(a) }
    { res = raiz(a) }

    IzqAVL( in a: avl(\alfa) ) --> res: avl(\alfa)                          O(1)
    { \not nil?(a) }
    { res = izq(a) }

    DerAVL( in a: avl(\alfa) ) --> res: avl(\alfa)                          O(1)
    { \not nil?(a) }
    { res = der(a) }

    NilAVL() --> res: avl(\alfa)                                            O(1)
    { true }
    { res = nil }

    AlturaAVL( in a: avl(\alfa) ) --> res: nat                              O(n)
    { true }
    { res = altura(a) }
    
    TamanoAVL( in a: avl(\alfa) ) --> res: nat                              O(n)
    { true }
    { res = tamano(a) }
    
    AgregarAVL( in elem: \alfa, inout a: avl(\alfa) )                   O(log n)
    { a0 = a }
    { if esta?( elem, inorder(a) ) then a = a0  
      else inoder(a) = agregarOrdenado( elem, inorder(a0) ) fi }

    EstaAVL?( in elem: \alfa, in a: avl(\alfa) ) --> res: bool         O(log n)
    { true }
    { res = esta?( elem, inorder(a) ) }
    
    ObtenerAVL( in elem: \alfa, in a: avl(\alfa) ) --> res: elem         O(c * log n) // c: orden maximo al comparar
    { EstaAVL?(elem, a) }                                                             // dos elementos de tipo \alfa
    { res = elem }  /* esto puede sonar de perogrullo, pero puede ocurrir que la comparacion por igualdad no tome a dos elementos
                      * iguales si ambos son excatamente iguales. Un caso tipico es el de los diccionarios donde \alfa 
                      * puede ser una tupla<clave x significado> pero la igualdad de dos tuplas se da solo si son iguales
                      * sus claves, indistintamente de sus significados
                      */
                      
    InorderAVL( in a: avl(\alfa) ) --> res: secu(\alfa)                   O(n)
    { true }
    { res = inorder(a) }

    /**
     *  Desde aqui hasta el final de la interfaz son funciones no exportadas
     *  pero se incluen en la interfaz solo a los efectos de su uso interno
     **/
      
    i_InorderAVL( in a: estr, inout s: secu(\alfa) )                  O(n)
    { s0 = s }
    { s = s0 & inorder(a) }
     
    _BinAVL( in elem: \alfa, in left,right,padre: avl(\alfa) ) --> res: avl(\alfa)      O(1)
    { true }
    { res = bin( left, elem, right ) }
    
    _AgregarAVL( in elem: \alfa, inout a: avl(\alfa), in previousLeft: bool )   O(log n)
    { a0 = a \and \not nil?(a) }
    { if esta?( elem, inorder(a) ) then a = a0  
      else inoder(a) = agregarOrdenado( elem, inorder(a0) ) fi }
      
    _Rebalancear( inout desdeAqui: estr, previousLeft, currentLeft: bool )      O(log n)
    { esABB(e) \and cadaHijoTiene�nicoPadre(e) \and punterosAPadreCorrectos(e) }
    { esABB(e) \and alturaDeCadaSubarbolDifiereEnMenosDe2(e) \and
      cadaHijoTiene�nicoPadre(e) \and punterosAPadreCorrectos(e) }
   
    _RebalanceoDesdeDonde?( in a: avl(\alfa) ) --> res: tupla< rebalanceo: bool  x  desdeAqui: avl(\alfa) >   O(log n)
                                                                            
    _RebalanceoLL( inout a: estr )                                          O(1)
    _RebalanceoRL( inout a: estr )                                          O(1)
    _RebalanceoLR( inout a: estr )                                          O(1)
    _RebalanceoRR( inout a: estr )                                          O(1)
    
    _EstaAVL?( in elem: \alfa, in a: avl(\alfa) ) --> res: tupla< esta?: bool x aqui: avl(\alfa) >        O(log n)
    { true }
    { res.esta? = esta?( elem, inorder(a) ) \and_l
      res.esta? =>L res.elem = elem }

Fin Interfaz

iNilAVL?( in a: estr ) --> res: bool                                        O(1)
    var ret: bool
    ret <-- (a = null)
    return ret

iRaizAVL( in a: avl(\alfa) ) --> res: \alfa                                 O(1)
    var ret: \alfa
    ret <-- a->valor
    return ret

iIzqAVL( in a: estr ) --> res: estr                                         O(1)
    var ret: estr
    ret <-- a->left
    return ret

iDerAVL( in a: estr ) --> res: estr                                         O(1)
    var ret: estr
    ret <-- a->right
    return ret

iNilAVL() --> res:  estr                                                    O(1)
    var ret: estr
    ret <-- null
    return ret

iAlturaAVL( in a: estr ) --> res: nat                                       O(n)
    var ret: nat
    if NilAVL?(a) then
        ret <-- 0
    else
        if AlturaAVL( a->left ) > AlturaAVL( a->right )
            ret <-- AlturaAVL( a->left ) + 1
        else
            ret <-- AlturaAVL( a->right ) + 1
        end
    end
    return ret
    
TamanoAVL( in a: avl(\alfa) ) --> res: nat                                  O(n)
    var ret: nat
    if NilAVL?(a) then
        ret <-- 0
    else
        ret <-- 1 + TamanoAVL( a->left ) + TamanoAVL( a->right )
    end
    return ret


iEstaAVL?( in elem: \alfa, in a: estr ) --> res: bool         O(log n)
    var aux:  tupla< esta?: bool x aqui: estr >
    aux <-- _EstaAVL?( elem, a )
    return aux.esta?

ObtenerAVL( in elem: \alfa, in a: avl(\alfa) ) --> res: elem            o( c * log n)
    var aux:  tupla< esta?: bool x aqui: estr >
    aux <-- _EstaAVL?( elem, a )                                O(log n)
    return aux.aqui->valor

i_EstaAVL?( in elem: \alfa, in a: estr ) --> res: tupla< esta?: bool x aqui: estr >        O(log n)
    var ret: tupla< esta?: bool x aqui: estr >
    if NilAVL?(a)
        ret.esta? <-- false
    else
        if eq( elem, a->valor )
            ret.esta? <-- true
            ret.aqui <-- a
        elseif \not lt( a->valor, elem )
            ret <-- _EstaAVL?( elem, a->left )
        else
            ret <-- _EstaAVL?( elem, a->right )
        end
    end

    return ret

iInorderAVL( in a: estr ) --> res: secu(\alfa)                                  O(n)
    var ret: secu(\alfa)
    ret <-- VaciaS()
    _InorderAVL( a, ret )                                           O(n)
    return ret
    
i_InorderAVL( in a: estr, inout s: secu(\alfa) )                                O(n)
    if \not NilAVL?(a)
        _InorderAVL( a->left, s )
        AgOS( s, a->valor )                                             O(1)
        _InorderAVL( a->right, s )
    end

iAgregarAVL( in elem: \alfa, inout a: estr )                                    O(log n)
    var nuevoNodo: nodo
    if ( a = null )
        a = i_BinAVL( elem, null, null, a )                                    O(1)
    elseif \not lt( a->valor, elem )
        i_AgregarAVL( elem, a, true )                                       O(log n)
    else
        i_AgregarAVL( elem, a, false )                                      O(log n)
    end

i_BinAVL( in elem: \alfa, in left,right,padre: estr ) --> res: estr             O(1)
    var ret: estr
    ret->valor <-- elem
    ret->left  <-- left
    ret->right <-- right
    ret->padre <-- padre
    return ret

i_AgregarAVL( in elem: \alfa, inout a: estr, in previousLeft: bool )            O(log n)  en el peor caso agrego el elemento al
                                                                                          final del arbol O(log n) llamadas a
                                                                                          _AgregarAVL y el rebalanceo tiene un
                                                                                          costo de O(log n) => O(log n) + O(log n)
                                                                                          = max( O(log n), O(log n)) = O(log n)
    if eq(a->valor, elem) / * Ver que el tipo \alfa debe proveer una operacion de
                            * comparacion por igualdad
                            */
        a->valor <-- elem  /* esto puede sonar de perogrullo, pero puede ocurrir que
                            * la comparacion por igualdad no tome a dos elementos
                            * iguales si ambos son excatamente iguales.
                            * Un caso tipico es el de los diccionarios donde \alfa 
                            * puede ser una tupla<clave x significado> pero la 
                            * igualdad de dos tuplas se da solo si son iguales
                            * sus claves, indistintamente de sus significados
                            */
    elseif \not lt( a->valor, elem )
        if NilAVL?(a->left) \or_l lt( a->left->valor, elem )                O(1)
            a->left = _BinAVL( elem, a->left, null, a )                     O(1)
            _Rebalancear( a->left, previousLeft, true )                 O(log n)
        else
            _AgregarAVL( elem, a->left, true )
        end
    else
        if NilAVL?(a->right) \or \not lt( a->right->valor, elem )
            a->right = _BinAVL( elem, null, a->right, a )
            _Rebalancear( a->right, previousLeft, false )                   O(log n)
        else
            _AgregarAVL( elem, a->right, false )
        end
    end


i_Rebalancear( inout desdeAqui: estr, previousLeft, currentLeft: bool )         O(log n)
    var hago: tupla< rebalanceo: bool  x  desdeAqui: estr >
    hago <-- _RebalanceoDesdeDonde?( desdeAqui )                            O(log n)
    if hago.rebalanceo 
        if previousLeft \and currentLeft
            _RebalanceoLL( hago.desdeAqui )                                     O(1)
        elseif previousLeft \and \not currentLeft
            _RebalanceoLR( hago.desdeAqui )                                     O(1)
        endif \not previousLeft \and currentLeft
            _RebalanceoRL( hago.desdeAqui )                                     O(1)
        elseif \not previousLeft \and \not currentLeft
            _RebalanceoRR( hago.desdeAqui )                                     O(1)
        end
    end


i_RebalanceoDesdeDonde?( in a: estr ) --> res: tupla< rebalanceo: bool  x  desdeAqui: estr >      O(log n) 
                                                                 porque en el peor caso agrego el elemento al final
                                                                 del arbol y el desbalance se produce en la raiz
    var prt: estr, rebalanceo: bool,
        ret: tupla< rebalanceo: bool  x  desdeAqui: estr >
    ptr <-- a
    rebalanceo <-- false
    while \not (ptr->padre = null) \and \not rebalanceo                             O(1) 
        rebalanceo <-- (AbsDiff( AlturaAVL( a->left ), AlturaAVL( a->right ) ) > 1)    O(1) 
        // esto evita que el puntero al nodo desde el que se va a hacer balanceo
        // quede desfasado
        if \not rebalanceo                                                          O(1)
            ptr <-- ptr->padre                                                      O(1)
        end
    endwhile
    ret.rebalanceo <-- rebalanceo                                                   O(1)
    if rebalanceo                                                                   O(1)
        ret.desdeAqui <-- ptr                                                       O(1)
    end
    
    return ret

i_RebalanceoLL( inout a: estr )                                             O(1)
    var hijoIzq: estr
    hijoIzq <-- a->left
    a->left <-- hijoIzq->right
    hijoIzq->right <-- a

i_RebalanceoRR( inout a: estr )                                             O(1)
    var hijoDer: estr
    hijoDer <-- a->right
    a->right <-- hijoDer->left
    hijoDer->left <-- a

i_RebalanceoLR( inout a: estr )                                             O(1)
    var hijoIzq,hijoIzqDer: estr
    hijoIzq <-- a->left
    hijoIzqDer <-- a->left->right
    a->left <-- hijoIzqDer->right
    hijoIzq->right <-- hijoIzqDer->left
    hijoIzqDer->right <-- a
    hijoIzqDer->left <-- hijoIzq

i_RebalanceoRl( inout a: estr )                                             O(1)
    var hijoDer,hijoDerIzq: estr
    hijoDer <-- a->right
    hijoDerIzq <-- a->right->left
    a->right <-- hijoDerIzq->left
    hijoDer->left <-- hijoDerIzq->right
    hijoDerIzq->left <-- a
    hijoDerIzq->right <-- hijoDer                    

==============================================================================================================================================================
datoAVL(clave, significado) se representon con estr
donde estr es tupla< c: clave x s: significado >
 
Interfaz
    se explica con especificacion DATOAVL(CLAVE,SIGNIFICADO)

    NewDaAVL( c: clave, s: significado ) --> res: datoAVL(clave, significado)   O(1)
    { true }
    { res = new(c,s) }

    NewCDaAVL( c: clave ) --> res: datoAVL(clave, significado)                  O(1)
    { true }
    { res = newC(c) }

    eq( a, b: datoAVL(clave, significado ) --> res: bool                        O(1)
    { true }
    { res = eq(a,b) }

    lt( a, b: datoAVL(clave, significado ) --> res: bool                       O(1)
    { true }
    { res = leq(a,b) }

    ClaveDaAVL( d: datoAVL(clave, significado ) ) --> res: clave                O(1)
    { true }
    { res = clave(d) }

    SigDaAVL( d: datoAVL(clave, significado ) ) --> res: significado            O(1)
    { sig?(d) }
    { res = sig(d) }

Fin Interfaz

NewDaAVL( c: clave, s: significado ) --> res: estr
    var ret: estr
    ret.c <-- c
    ret.s <-- s
    return ret

NewCDaAVL( c: clave ) --> res: estr
    var ret: estr
    ret.c <-- c
    return ret

eq( a, b: estr ) --> res: bool                                              O(1)
    var ret: bool
    ret <-- (a.c = b.c)
    return ret

lt( a, b: estr ) --> res: bool                                              O(1)
    var ret: bool
    ret <-- (a.c < b.c)
    return ret

ClaveDaAVL( d: estr ) ) --> res: clave                                      O(1)
    return d.c

SigDaAVL( d: estr ) --> res: significado                                    O(1)
    return d.s

==============================================================================================================================================================
diccAVL( clave,significado ) se representa con estr
donde estr es avl( datoAVL(clave,significado) )

Rep: estr --> bool
Rep(e) = true

Abs: estr --> diccAVL( clave,significado )                  Rep(e)
(\paratodo e:estr) Abs(e) = d: diccAVL( clave,significado ) /
                            (\paratodo c: clave) ( (\existe sig: significado ) def?(c,d) = esta?( new(c,sig), inorder(d) ) 
                                                     \and_l  def?(c,d) =>L obtener(c,d) = sig
                            )
                            
claves: secu(datoAVL(clave,significado)) --> conj(clave)
claves(s) = vacia?(s) then <>
            else clave( prim(s) ) . claves( fin(s) )
            fi

Interfaz
    se explica con especificacion DICCIONARIO(clave,significado)
    usa interfaces BOOL, NAT, CONJUNTO(clave)
    genero diccAVL(clave,significado)
    
    operaciones:

    DefDAVL?( in c: clave, in d: diccAVL(clave,significado) ) --> res: bool                   O(log n)
    { true }
    { res = def?(c,d) }
    
    ObtenerDAVL(  in c: clave, in d: diccAVL(clave,significado) ) --> res: significado         O(c *log n) // c: orden max de comparaciones 
    { def?(c,d) }                                                                                          // de la clave
    { res = obtener(c,d) }

    VacioDAVL() --> res:  diccAVL(clave,significado)                                          O(1)
    { true }
    { res = vacio )
    
    DefinirDAVL( in c: clave, in sig: significado, inout d: diccAVL(clave,significado) )      O(log n)
    { d0 = d }
    { d = definir(c, sig, d0) }
    
    BorrarDAVL(  in c: clave, inout d: diccAVL(clave,significado) )                           O(log n)
    { def?(c,d) }
    { res = borrar(c,d) }
    
    ClavesDAVL( in d: diccAVL(clave,significado) ) --> res: conj(clave)                       O(n)
    { true }
    { res = claves(d) }

     /**
     *  Desde aqui hasta el final de la interfaz son funciones no exportadas
     *  pero se incluen en la interfaz solo a los efectos de su uso interno
     **/

    _ExtraerClaves( in s: secu(datoAVL(clave,significado)) ) --> res: conj(clave)                  O(n)
    { true }
    { res = claves( s ) }

Fin Interfaz



iDefDAVL?( in c: clave, in d: estr ) --> res: bool                   O(log n)
    var ret: bool, dato: datoAVL(clave,significado)
    dato <-- NewCDaAVL(c)
    ret <-- EstaAVL?( dato, d )                                 O(log n)
    return ret
    
iObtenerDAVL(  in c: clave, in d: estr ) --> res: significado        O(c *log n) // c: orden max de comparaciones 
    var dato: datoAVL(clave,significado)                                         // de la clave
    dato <-- NewCDaAVL(c)
    dato <-- ObtenerAVL( dato, d )                              O(c * log n)
    return SigDaAVL(d)

iVacioDAVL() --> res:  estr                                                 O(1)
    var ret: estr                 
    ret <-- NilAVL()
    return ret

iDefinirDAVL( in c: clave, in sig: significado, inout d: estr )                 O(log n)
    var dato: datoAVL(clave,significado)
    dato <-- new(c,sig)
    AgregarAVL( dato, d )                                                   O(log n)


iBorrarDAVL(  in c: clave, inout d: estr )                           O(log n)
    var dato: datoAVL(clave,significado)
    dato <-- newC(c)
    BorrarAVL(dato,d)

iClavesDAVL( in d: estr ) --> res: conj(clave)                       O(n)
    var s: secu(datoAVL(clave,significado))
    s <-- InorderAVL(d)                         O(n)
    return _ExtraerClaves(s)                    O(n)

i_ExtraerClaves( in s: secu( datoAVL(clave,significado) ) ) --> ret: conj(clave)        O(n)
    var res: conj(clave), aux: secu( datoAVL(clave,significado) )
    res <-- VacioC()                                                            O(1)
    aux <-- Copiar(s)                                                           O(n)
    while \not VaciaS?(aux)
        AgregarC( ClaveDaAVL( PrimS(s) ), res )                                 O(1)
        FinS(s)                                                                 O(1)
    endwhile

    return res


