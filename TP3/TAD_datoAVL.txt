TAD DATOAVL(CLAVE,SIGNIFICADO)

    exporta     igualdad observacional, obervadores, generadores, clave, sig
    genero datoAVL(clave,significado)
    usa BOOL
    igualdad observacional
        paratodo a1,a2: datoAVL(clave,significado) (a1 = a2) <=>  eq(a1,a2)

    observadores
        eq: datoAVL(clave,significado)  x  datoAVL(clave,significado)  --> bool

    generadores
        new: clave  x  significado --> datoAVL(clave,significado)
        newC: clave --> datoAVL(clave,significado)
    otras operaciones
        clave: datoAVL(clave,significado) --> clave
        sig: datoAVL(clave,significado) d --> significado             sig(d)?
        sig?: datoAVL(clave,significado) --> bool
        lt: datoAVL(clave,significado)  x  datoAVL(clave,significado)  --> bool

    axiomas
        \paratodo c1,c2: clave, s1,s2: significado

        eq( new(c1,s1), new(c2,s2) ) = (c1 = c2)
        eq( newC(c1), newC(c2) ) = (c1 = c2)

        lt( new(c1,s1), new(c2,s2) ) = (c1 < c2)
        lt( newC(c1), newC(c2) ) = (c1 < c2)

        clave( new(c1,s1) ) = c1
        clave( newC(c1) ) = c1

        sig( new(c1, s1) ) = s1
FIN TAD
