#!/usr/bin/perl

use strict;
use warnings;

my $MAX_LENGTH = $ARGV[0] || die ("Necesita pasar la long. maxima.\n");
my $MOD_FACTOR = $ARGV[1] || die ("Necesita pasar el tama�o del array\n");
my @char =  'a' .. 'z';

sub inc($) {
    my $letter = shift;
    my $overflow;

    if ($letter eq  'z') {
        $overflow = 1;
        $letter = 'a';
    } else {
        $overflow = 0;
        $letter = $char[ ord($letter) - ord('a') + 1];
    };
    
    return ($letter,$overflow);
};


sub thisSub($) {
    my $length = shift;

    my @return;
    for my $j (0..$length-1) {
        $return[$j] = $char[0];
    }

    my $sub = sub {
        my $i = 0;
        my $moreIncrement = 1;
        my ($overflow, $stop) = (0,0);
        while ( $moreIncrement && ! $stop  ) {
            ($return[$i], $overflow) = inc( $return[$i] );
            if ($overflow) {
                $i++;
                $moreIncrement = 1;
            } else {
                $moreIncrement = 0;
            };

            $stop = ($i > $#return) && $moreIncrement;
        }
        return (join( '', @return ), $stop );
    };

    return $sub;
}

sub hash1($) {
    my $string = shift;
    my $result = 0;
    my $i = 0;
    for my $char ( split //, $string ) {
        $result += ( ord($char) - ord('a') + 1) * (2**$i++);
    }
    
    return ($result % $MOD_FACTOR);
};

my %result;


for my $length ( 1 .. $MAX_LENGTH ) {
    my ($string,$stop);
    my $p = thisSub($length);

    while( ! $stop ) {
        ($string,$stop) = $p->();
        my $hash = hash1($string);
        $result{ $hash }++;
    };
};

foreach (sort { $a <=> $b } keys %result ) {
    print "$_,$result{$_}\n";
}
